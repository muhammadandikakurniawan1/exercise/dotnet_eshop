﻿using FrameworkCore.Domains.User.Infrastructure.Persistence.TableModels;
using FrameworkCore.Domains.User.ValueObjects;
using FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Infrastructure.Persistence.Dao
{
    public class AppUserMsSqlDaoImpl : BaseEshopMsSqlDao<AppUser>, IAppUserDao
    {
        public AppUserMsSqlDaoImpl(EshopMsSqlDbContext dbContext) : base(dbContext)
        {

        }

        public AppUser? FindById(string id)
        {
            return base.Find((entity) => entity.Id.Equals(id)).FirstOrDefault();
        }

        public async Task<AppUser> SaveAsync(AppUser data)
        {
           var insertRes = _context.Set<AppUser>().Add(data);
           await _context.SaveChangesAsync();
           return insertRes.Entity;
        }

        public AppUser? FindByAccountId(UserType userType, string id)
        {

            switch (userType)
            {
                case UserType.GOOGLE:
                    return base.Find((entity) => entity.GoogleAccountId.Equals(id)).FirstOrDefault();
                case UserType.APP:
                    return FindById(id);
                default: return null;
            }

            return null;
        }
    }
}
