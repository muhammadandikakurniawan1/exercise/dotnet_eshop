﻿using FrameworkCore.Domains.User.Infrastructure.Persistence.TableModels;
using FrameworkCore.Domains.User.ValueObjects;
using FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Infrastructure.Persistence.Dao
{
    public interface IAppUserDao
    {
        public Task<AppUser> SaveAsync(AppUser data);
        public AppUser? FindById(string id);
        public AppUser? FindByAccountId(UserType userType,string id);
    }
}
