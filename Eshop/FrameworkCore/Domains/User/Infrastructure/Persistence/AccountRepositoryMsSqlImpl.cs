﻿using FrameworkCore.Domains.User.Aggregates.Account;
using FrameworkCore.Domains.User.Infrastructure.Persistence.Dao;
using FrameworkCore.Domains.User.Infrastructure.Persistence.TableModels;
using FrameworkCore.Domains.User.Repositories;
using FrameworkCore.Domains.User.ValueObjects;
using FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext;
using System.Transactions;

namespace FrameworkCore.Domains.User.Infrastructure.Persistence
{
    public class AccountRepositoryMsSqlImpl : IAccountRepository
    {
        private IAppUserDao _appUserDao;
        public AccountRepositoryMsSqlImpl(
            EshopMsSqlDbContext dbContext,
            IAppUserDao appUserDao
        )
        {
            _appUserDao = appUserDao;
        }

        public AccountAggregate? FindByUserId(string id)
        {
            var appUserData = _appUserDao.FindById(id);
            if(appUserData == null)
            {
                return null;
            }

            return new AccountAggregate(appUserData.ToEntity());
        }

        public async Task<AccountAggregate> SaveAsync(AccountAggregate aggregateData)
        {
            var listAggregateEvent = aggregateData.GetEvents();

            using (var scope = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            {

                try
                {
                    foreach (var e in listAggregateEvent)
                    {
                        if (e is AddNewUserEvent)
                        {
                            var aggEvent = e as AddNewUserEvent;
                            await EventHandler(aggEvent!);
                        }
                    }

                    scope.Complete();
                    scope.Dispose();
                }
                catch(Exception)
                {
                    scope.Dispose();
                    throw;
                }
            }

            return aggregateData;
        }

        public AccountAggregate? FindByOauthAccountId(UserType userType, string id)
        {
            var userData = _appUserDao.FindByAccountId(userType, id);
            var userNotfound = userData == null;
            if (userNotfound)
            {
                return null;
            }

            return new AccountAggregate(userData!.ToEntity());
        }

        private async Task<AppUser> EventHandler(AddNewUserEvent e)
        {
            var eventData = e.GetEventData();
            var rowData = new TableModels.AppUser(eventData);
            var saveRes =  await this._appUserDao.SaveAsync(rowData);

            return saveRes;
        }
    }
}
