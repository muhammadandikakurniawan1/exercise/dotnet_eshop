﻿using FrameworkCore.Domains.User.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Infrastructure.Persistence.TableModels;

[Table("app_user")]
[Index("Email", Name = "app_user_email_idx")]
[Index("FullName", Name = "app_user_full_name_idx")]
public partial class AppUser
{
    [Key]
    [Column("id")]
    [StringLength(255)]
    [Unicode(false)]
    public string Id { get; set; } = "";

    [Column("full_name")]
    [StringLength(255)]
    [Unicode(false)]
    public string FullName { get; set; } = ""!;

    [Column("email")]
    [StringLength(255)]
    [Unicode(false)]
    public string Email { get; set; } = "";

    [Column("password", TypeName = "text")]
    public string Password { get; set; } = "";

    [Column("created_at")]
    public long CreatedAt { get; set; }

    [Column("updated_at")]
    public long UpdatedAt { get; set; }

    [Column("deleted_at")]
    public long? DeletedAt { get; set; }

    [Column("google_account_id")]
    [StringLength(255)]
    [Unicode(false)]
    public string GoogleAccountId { get; set; } = "";

    [Column("google_account_name")]
    [StringLength(255)]
    [Unicode(false)]
    public string GoogleAccountName { get; set; } = "";

    [Column("google_account_email")]
    [StringLength(255)]
    [Unicode(false)]
    public string GoogleAccountEmail { get; set; } = "";

    [Column("google_account_picture")]
    [StringLength(255)]
    [Unicode(false)]
    public string GoogleAccountPicture { get; set; } = "";

    public AppUser() { }
    public AppUser(UserEntity entityData) {
        this.Id = entityData.Id?.ToString() ?? "";
        this.FullName = entityData.FullName;
        this.Email = entityData.Email;
        this.Password = entityData.Password;
        this.CreatedAt = entityData.CreatedAt;
        this.UpdatedAt = entityData.UpdatedAt;
        this.DeletedAt = entityData.DeletedAt;

        var googleAccountIsExists = !string.IsNullOrEmpty(entityData.GoogleAccount.AccountId);
        if (googleAccountIsExists)
        {
            this.GoogleAccountId = entityData.GoogleAccount.AccountId;
            this.GoogleAccountName = entityData.GoogleAccount.Name;
            this.GoogleAccountEmail = entityData.GoogleAccount.Email;
            this.GoogleAccountPicture = entityData.GoogleAccount.PictureUrl;
        }
    }

    public UserEntity ToEntity()
    {
        var entityData = new UserEntity
        {
            Id = Guid.Parse(this.Id),
            FullName = this.FullName,
            Email = this.Email,
            Password = this.Password,

            CreatedAt = this.CreatedAt,
            UpdatedAt = this.UpdatedAt,
            DeletedAt = this.DeletedAt,
        };

        if(!string.IsNullOrEmpty(this.GoogleAccountId))
        {
            entityData.GoogleAccount = new ValueObjects.OAuthUser
            {
                AccountId = this.GoogleAccountId,
                Name = this.GoogleAccountName,
                Email = this.GoogleAccountEmail,
                PictureUrl = this.GoogleAccountPicture,
            };
        }

        return entityData;
    }

    public string GetDisplayName()
    {
        var isUsernameExists = !string.IsNullOrEmpty(this.FullName) && !string.IsNullOrWhiteSpace(this.FullName);
        if (isUsernameExists) return this.FullName;

        var isGoogleAccountExists = !string.IsNullOrEmpty(this.GoogleAccountName) && !string.IsNullOrWhiteSpace(this.GoogleAccountName);
        if (isUsernameExists) return this.GoogleAccountName;

        return "";
    }
}

