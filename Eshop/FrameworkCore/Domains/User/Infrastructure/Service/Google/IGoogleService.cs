﻿using FrameworkCore.Domains.User.Infrastructure.Service.Google.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Infrastructure.Service.Google
{
    public interface IGoogleOAuth2Service
    {
        public Task<OauthUserInfoModel> GetOAuthUserInfo(string accessToken);
    }
}
