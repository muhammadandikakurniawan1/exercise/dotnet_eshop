﻿using FrameworkCore.Domains.User.ValueObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Infrastructure.Service.Google.Models
{
    public class OauthUserInfoModel
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; } = "";

        [JsonProperty("email", NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; } = "";

        [JsonProperty("verified_email", NullValueHandling = NullValueHandling.Ignore)]
        public string VerifiedEmail { get; set; } = "";

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; } = "";

        [JsonProperty("given_name", NullValueHandling = NullValueHandling.Ignore)]
        public string GivenName { get; set; } = "";

        [JsonProperty("family_name", NullValueHandling = NullValueHandling.Ignore)]
        public string FamilyName { get; set; } = "";

        [JsonProperty("picture", NullValueHandling = NullValueHandling.Ignore)]
        public string Picture { get; set; } = "";

        [JsonProperty("locale", NullValueHandling = NullValueHandling.Ignore)]
        public string Locale { get; set; } = "";

        [JsonProperty("error", NullValueHandling = NullValueHandling.Ignore)]
        public ErrorOauthUserInfoModel? error { get; set; }

        public OAuthUser ToOAuthUserValueObject()
        {
            return new OAuthUser()
            {
                AccountId = this.Id,
                Name = this.GivenName,
                PictureUrl = this.Picture,
                Email = this.Email
            };
        }
    }

    public class ErrorOauthUserInfoModel
    {
        [JsonPropertyName("code")]
        public int Code { get; set; } = 0;

        [JsonPropertyName("message")]
        public string Message { get; set; } = "";

        [JsonPropertyName("status")]
        public string Status { get; set; } = "";
    }
}