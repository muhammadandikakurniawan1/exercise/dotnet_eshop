﻿using FrameworkCore.Domains.User.Infrastructure.Service.Google.Models;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Infrastructure.Service.Google
{
    public class GoogleOAuth2Service : IGoogleOAuth2Service
    {
        HttpClient _httpClient;
        public GoogleOAuth2Service(IHttpClientFactory clientFactory)
        {
            this._httpClient = clientFactory.CreateClient("HttpClient");
        }
        
        public async Task<OauthUserInfoModel> GetOAuthUserInfo(string accessToken)
        {
            var result = new OauthUserInfoModel();
            var query = new Dictionary<string, string>
            {
                ["alt"] = "json",
                ["access_token"] = accessToken,
            };
            var uri = QueryHelpers.AddQueryString("https://www.googleapis.com/oauth2/v1/userinfo", query);
            using(var response = await _httpClient.GetAsync(uri))
            {
                var responseJsonString = await response.Content.ReadAsStringAsync();
                var responseBody = JsonConvert.DeserializeObject<OauthUserInfoModel>(responseJsonString) ?? new OauthUserInfoModel();
                if (!response.IsSuccessStatusCode)
                {
                    var errorMsgBuilder = new StringBuilder();
                    errorMsgBuilder.Append("failed get google oauth user info");

                    if(responseBody.error != null) errorMsgBuilder.Append($", message: {responseBody.error.Message}");

                    throw new Exception(errorMsgBuilder.ToString());
                }

                result = responseBody;
            }
            return result;
        }
    }
}
