﻿using FrameworkCore.Domains.User.Entities;
using FrameworkCore.Primitive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Aggregates.Account
{
    public class AccountAggregate : Aggregate
    {
        private UserEntity userData;

        public AccountAggregate(UserEntity userData)
        {
            this.userData = userData;
        }

        public void CreateNewAccount()
        {
            long createdAt = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();

            this.userData.Id = Guid.NewGuid();
            this.userData.CreatedAt = createdAt;
            this.userData.UpdatedAt = createdAt;
            var newEvent = new AddNewUserEvent(this.userData);
            this.AddEvent(newEvent);
        }

        public UserEntity GetUser()
        {
            return this.userData;
        }
    }
}
