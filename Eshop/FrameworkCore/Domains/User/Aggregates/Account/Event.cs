﻿using FrameworkCore.Domains.User.Entities;
using FrameworkCore.Primitive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Aggregates.Account
{
    public class AddNewUserEvent : AggregateEvent<UserEntity>
    {
        public AddNewUserEvent(UserEntity data) : base(data) {
        }
    }

    public class SetUserActiveEvent : AggregateEvent<bool>
    {
        public SetUserActiveEvent(bool data) : base(data)
        {
        }
    }
}
