﻿using FrameworkCore.Domains.User.Entities;
using FrameworkCore.Domains.User.Usecases.User.Models;
using FrameworkCore.Domains.User.ValueObjects;
using FrameworkCore.Pkg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Usecases.User
{
    public interface IUserUsecase
    {
        public Task<BaseResponseModel<UserEntity>> LoginOauthAsync(LoginOAuthRequestModel request);
        public Task<BaseResponseModel<UserEntity>> GetUserByAccountId(UserType userType, string id);
    }
}
