﻿using FrameworkCore.Domains.User.ValueObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Usecases.User.Models
{
    public class LoginOAuthRequestModel
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; } = "";

        [JsonProperty("login_type")]
        public UserType LoginType { get; set; }
    }
}
