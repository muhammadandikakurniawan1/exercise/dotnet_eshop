﻿using FrameworkCore.Domains.User.Entities;
using FrameworkCore.Domains.User.Factories;
using FrameworkCore.Domains.User.Infrastructure.Service.Google;
using FrameworkCore.Domains.User.Infrastructure.Service.Google.Models;
using FrameworkCore.Domains.User.Repositories;
using FrameworkCore.Domains.User.Usecases.User.Models;
using FrameworkCore.Domains.User.ValueObjects;
using FrameworkCore.Pkg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Usecases.User
{
    public class UserUsecase : IUserUsecase
    {
        private IAccountRepository _accountRepository;
        private IGoogleOAuth2Service _googleOAuth2Service;
        public UserUsecase(
            IAccountRepository accountRepository,
            IGoogleOAuth2Service googleOAuth2Service
        )
        {
            _accountRepository = accountRepository;
            _googleOAuth2Service = googleOAuth2Service;
        }

        public async Task<BaseResponseModel<UserEntity>> LoginOauthAsync(LoginOAuthRequestModel request)
        {
            var result = new BaseResponseModel<UserEntity>();
            try
            {
                OauthUserInfoModel oauthUserInfoData;
                switch (request.LoginType)
                {
                    case UserType.GOOGLE:
                        oauthUserInfoData = await _googleOAuth2Service.GetOAuthUserInfo(request.AccessToken);
                        break;

                    default: throw new ArgumentException("invalid login type");
                }


                var accountData = _accountRepository.FindByOauthAccountId(request.LoginType, oauthUserInfoData.Id);
                var accountNotfound = accountData == null;

                if (accountNotfound)
                {
                    accountData = AccountFactory.BuildFromOAuthAccount(request.LoginType, oauthUserInfoData.ToOAuthUserValueObject());
                    accountData.CreateNewAccount();
                    accountData = await _accountRepository.SaveAsync(accountData);
                }

                result.Data = accountData!.GetUser();
                result.IsSuccess = true;
                result.Message = "success";
                result.StatusCode = "USR200";
                result.HttpCode = System.Net.HttpStatusCode.OK;
            }
            catch(Exception ex)
            {
                result.SetInternalServerError(ex);
            }

            return result;
        }

        public async Task<BaseResponseModel<UserEntity>> GetUserByAccountId(UserType userType, string id)
        {
            var result = new BaseResponseModel<UserEntity>();

            try
            {
                var accountData = _accountRepository.FindByOauthAccountId(userType, id);
                var userNotfound = accountData == null;
                if (userNotfound)
                {
                    result.Message = "user not found";
                    result.ErrorMessage = "user not found, Unauthorize";
                    result.StatusCode = "USR403";
                    result.HttpCode = System.Net.HttpStatusCode.Unauthorized;
                    return await Task.FromResult(result);
                }


                result.Data = accountData!.GetUser();
                result.IsSuccess = true;
                result.Message = "success";
                result.StatusCode = "USR200";
                result.HttpCode = System.Net.HttpStatusCode.OK;
            }
            catch(Exception ex)
            {
                result.SetInternalServerError(ex);
            }

            return await Task.FromResult(result);
        }
    }
}
