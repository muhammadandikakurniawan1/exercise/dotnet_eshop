﻿using FrameworkCore.Domains.User.Aggregates.Account;
using FrameworkCore.Domains.User.Entities;
using FrameworkCore.Domains.User.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Factories
{
    public static class AccountFactory
    {
        public static AccountAggregate BuildFromOAuthAccount(UserType userType , OAuthUser oauthUserData)
        {
            var userEntityData = new UserEntity();
            switch (userType)
            {
                case UserType.GOOGLE:
                    userEntityData.GoogleAccount = oauthUserData;
                    break;
                default : throw new ArgumentException("invalid account type");
            }
            
            var aggregateData = new AccountAggregate(userEntityData);

            return aggregateData;
        }
    }
}
