﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.ValueObjects;

public class OAuthUser
{
    public string Name { get; set; } = "";
    public string AccountId { get; set; } = "";
    public string Email { get; set; } = "";
    public string PictureUrl { get; set; } = "";
}

public enum UserType
{
    APP, GOOGLE
}
