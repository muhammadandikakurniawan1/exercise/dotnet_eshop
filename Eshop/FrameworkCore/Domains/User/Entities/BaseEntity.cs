﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Entities
{
    public class BaseEntity
    {
        public long CreatedAt { get; set; } = 0;
        public long UpdatedAt { get; set; } = 0;
        public long? DeletedAt { get; set; }
    }
}
