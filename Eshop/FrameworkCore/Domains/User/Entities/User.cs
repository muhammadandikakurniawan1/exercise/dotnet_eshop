﻿using FrameworkCore.Domains.User.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Entities
{
    public class UserEntity : BaseEntity
    {
        public Guid? Id { get; set; }
        public string FullName { get; set; } = "";
        public string Email { get; set; } = "";
        public string Password { get; set; } = "";
        public OAuthUser GoogleAccount { get; set; } = new OAuthUser();
    }
}
