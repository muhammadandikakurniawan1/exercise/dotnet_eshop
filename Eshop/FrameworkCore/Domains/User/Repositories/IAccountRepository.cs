﻿using FrameworkCore.Domains.User.Aggregates.Account;
using FrameworkCore.Domains.User.Entities;
using FrameworkCore.Domains.User.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.User.Repositories
{
    public interface IAccountRepository
    {
        public Task<AccountAggregate> SaveAsync(AccountAggregate data);
        public AccountAggregate? FindByUserId(string id);
        public AccountAggregate? FindByOauthAccountId(UserType userType ,string id);
    }
}
