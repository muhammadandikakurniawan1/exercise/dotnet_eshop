﻿using FrameworkCore.Domains.Cart.Aggregates.Cart;
using FrameworkCore.Domains.Cart.Entities;
using FrameworkCore.Pkg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Cart.Usecases.Cart;

public interface ICartUsecase
{
    public Task<BaseResponseModel<CartAggregate>> AddItem(CartItemEntity item);
    public Task<BaseResponseModel<CartAggregate>> GetByUserId(string userId);
}
