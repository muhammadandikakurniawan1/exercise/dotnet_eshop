﻿using FrameworkCore.Domains.Cart.Aggregates.Cart;
using FrameworkCore.Domains.Cart.Entities;
using FrameworkCore.Domains.Cart.Infrastructure.Service.Store;
using FrameworkCore.Domains.Cart.Infrastructure.Service.User;
using FrameworkCore.Domains.Cart.Repositories;
using FrameworkCore.Pkg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Cart.Usecases.Cart;

public class CartUsecase : ICartUsecase
{
    private IUserUsecase _userUsecase;
    private IProductUsecase _productUsecase;
    private ICartRepository _cartRepository;
    public CartUsecase(
     IUserUsecase userUsecase,
     IProductUsecase productUsecase,
     ICartRepository cartRepository
    )
    {
        _userUsecase = userUsecase;
        _productUsecase = productUsecase;
        _cartRepository = cartRepository;
    }
    public async Task<BaseResponseModel<CartAggregate>> AddItem(CartItemEntity item)
    {
        var result = new BaseResponseModel<CartAggregate>();

        try
        {
            var cartAggregate = _cartRepository.FindByUserIdAndProductId(item.UserId, item.Product.Id);
            if (item.Product.Id <= 0) throw new ArgumentException("product not found");
            var productData = await _productUsecase.GetProductById(item.Product.Id);
            if (productData == null) throw new ArgumentException("product not found");
            item.Product = productData;
            item.Store.Id = productData.Store.Id;

            cartAggregate.AddItem(item);
            cartAggregate = await _cartRepository.SaveAsync(cartAggregate);

            result.IsSuccess = true;
            result.Message = "success";
            result.StatusCode = "CART200";
            result.HttpCode = System.Net.HttpStatusCode.OK;
            result.Data = cartAggregate;
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex);
            result.SetInternalServerError(ex);
        }

        return result;
    }

    public async Task<BaseResponseModel<CartAggregate>> GetByUserId(string userId)
    {
        var result = new BaseResponseModel<CartAggregate>();
        try
        {
            var cartAggregate = _cartRepository.FindByUserId(userId);
            var cartEntity = cartAggregate.GetCart();
            var listProductId = cartEntity.Items.Select(i => i.Product.Id).ToArray();
            var items = await _productUsecase.GetListProductById(listProductId);

            for(var i = 0; i < cartEntity.Items.Count; i++)
            {
                var item = cartEntity.Items[i];
                if (item == null) continue;
                ProductEntity? itemData;
                var isExists = items.TryGetValue(item.Product.Id, out itemData);
                if (isExists && itemData != null)
                {
                    cartEntity.Items[i].Product = itemData;
                    var itemDataClone = cartEntity.Items[i];
                    cartAggregate.AddProductPerStore(itemDataClone);
                }
            }

            result.Data = cartAggregate;
            result.IsSuccess = true;
            result.Message = "success";
            result.StatusCode = "CART200";
            result.HttpCode = System.Net.HttpStatusCode.OK;
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex);
            result.SetInternalServerError(ex);
        }
        return await Task.FromResult(result);
    }
}
