﻿using FrameworkCore.Domains.User.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Cart.Entities
{
    public class UserEntity : BaseEntity
    {
        public string Id { get; set; } = "";
        public string Name { get; set; } = "";
        public string Email { get; set; } = "";
        public string Picture { get; set; } = "";
    }
}
