﻿using FrameworkCore.Domains.Cart.ValueObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Cart.Entities
{
    public class ProductEntity
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public int Id { get; set; } = 0;
        public string Name { get; set; } = "";
        public ProductPrice Price { get; set; } = new ProductPrice();
        public StoreEntity Store { get; set; } = new StoreEntity();
        public string Image { get; set; } = "";
    }
}
