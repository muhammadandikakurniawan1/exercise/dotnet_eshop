﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Cart.Entities;

public class CartEntity
{
    public string UserId { get; set; } = "";
    public List<CartItemEntity> Items { get; set; } = new List<CartItemEntity>();
}

public class CartItemEntity
{
    [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
    public int Id { get; set; } = 0;

    [JsonProperty("user_id", NullValueHandling = NullValueHandling.Ignore)]
    public string UserId { get; set; } = "";

    [JsonProperty("product", NullValueHandling = NullValueHandling.Ignore)]
    public ProductEntity Product { get; set; } = new ProductEntity();

    [JsonProperty("qty", NullValueHandling = NullValueHandling.Ignore)]
    public int Qty { get; set; } = 0;

    public StoreEntity Store { get; set; } = new StoreEntity();
    public long CreatedAt { get; set; }
    public long UpdatedAt { get; set; }
    public long? DeletedAt { get; set; }

    public CartItemEntity() { }

    public CartItemEntity(ProductEntity data)
    {
        
    }
}

