﻿using FrameworkCore.Domains.Cart.Entities;
using FrameworkCore.Primitive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Cart.Aggregates.Cart;

public class AddItemEvent : AggregateEvent<CartItemEntity>
{
    public AddItemEvent(CartItemEntity data) : base(data)
    {

    }
}

public class UpdateItemQtyEvent : AggregateEvent<CartItemEntity>
{
    public UpdateItemQtyEvent(CartItemEntity data) : base(data)
    {

    }
}