﻿using FrameworkCore.Domains.Cart.Entities;
using FrameworkCore.Primitive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Cart.Aggregates.Cart;

public class CartAggregate : Aggregate
{
    private CartEntity cart = new CartEntity();
    private List<StoreEntity> listStore = new List<StoreEntity>();
    private Dictionary<string, int> mapStoreId = new Dictionary<string, int>();

    
    public CartAggregate()
    {

    }

    public CartAggregate(CartEntity cart)
    {
        this.cart = cart;
    }

    public List<StoreEntity> GetItemPerStore()
    {
        return listStore;
    }

    public void AddProductPerStore(CartItemEntity item)
    {
        int storeIdx;
        string storeId = item.Store.Id.ToString()!;
        var isExists = mapStoreId.TryGetValue(storeId, out storeIdx);
        if (!isExists)
        {
            var idx = listStore.FindIndex(s => s.Id.ToString()!.Equals(storeId));
            var storeIsExists = idx > -1;
            if (storeIsExists)
            {
                storeIdx = idx;
            }
            else
            {
                listStore.Add(item.Product.Store);
                storeIdx = listStore.Count - 1;
            }
            mapStoreId.Add(storeId, storeIdx);
        }

        listStore[storeIdx].ListItem.Add(item);
    }

    public CartEntity GetCart()
    {
        return cart;
    }
    public void AddItem(CartItemEntity newItem)
    {
        long now = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
        newItem.UserId = cart.UserId;
        var currentItemIdx = this.cart.Items.FindIndex(i => i.Product.Id == newItem.Product.Id);
        var isItemAlreadyExists = currentItemIdx > -1;
        if (isItemAlreadyExists)
        {
            var currentItem = this.cart.Items[currentItemIdx]!;
            currentItem.UpdatedAt = now;
            currentItem!.Qty += newItem.Qty;
            this.cart.Items[currentItemIdx] = currentItem;
            var updateItemQtyEvent = new UpdateItemQtyEvent(currentItem);
            this.AddEvent(updateItemQtyEvent);
            return;
        }
        newItem.CreatedAt = now;
        newItem.UpdatedAt = now;
        this.cart.Items.Add(newItem);
        var addItemEvent = new AddItemEvent(newItem);
        this.AddEvent(addItemEvent);
    }
}
