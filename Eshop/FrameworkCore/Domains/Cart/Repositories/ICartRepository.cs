﻿using FrameworkCore.Domains.Cart.Aggregates.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Cart.Repositories;

public interface ICartRepository
{
    public Task<CartAggregate> SaveAsync(CartAggregate aggregateData);
    public CartAggregate FindByUserIdAndProductId(string userId, int productId);
    public CartAggregate FindByUserId(string userId);
}
