﻿using FrameworkCore.Domains.Cart.Entities;
using FrameworkCore.Domains.Store.Infrastructure.Persistence.TableModels;
using FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Cart.Infrastructure.Service.Store;

public class ProductUsecaseSqlServerImpl : IProductUsecase
{
    private EshopMsSqlDbContext _dbCtx;
    public ProductUsecaseSqlServerImpl(EshopMsSqlDbContext dbCtx)
    {
        _dbCtx = dbCtx;
    }

    public async Task<Dictionary<int, ProductEntity>> GetListProductById(int[] ids)
    {
        var result = new Dictionary<int, ProductEntity>();
        var listProduct = await _dbCtx.Products.Where(prd => ids.Contains(prd.Id))
            .Include(p => p.ProductPrices)
            .Include(p => p.ProductImages)
            .Include(p => p.Store)
            .ToListAsync();

        listProduct.ForEach(prd =>
        { 
            var prdEntityData = ParseProductEntity(prd);
            result.Add(prd.Id, prdEntityData);
        });
        return result;
    }

    public async Task<ProductEntity?> GetProductById(int id)
    {
        ProductEntity prdEntityData;
        var product = _dbCtx.Products.Where(prd => prd.Id == id)
            .Include(p => p.ProductPrices)
            .Include(p => p.Store)
            .FirstOrDefault();
        if (product == null) return null;

        prdEntityData = ParseProductEntity(product);
        return await Task.FromResult(prdEntityData);

    }

    public ProductEntity ParseProductEntity(Product product)
    {
        var prdEntityData = new ProductEntity();
        prdEntityData.Id = product.Id;
        prdEntityData.Name = product.Name;
        prdEntityData.Image = (product.ProductImages.FirstOrDefault() ?? new ProductImage()).Url;
        prdEntityData.Price = new ValueObjects.ProductPrice()
        {
            Currency = product.ProductPrices.First().CurrencyCode,
            Value = product.ProductPrices.First().Value,
        };
        prdEntityData.Store = new StoreEntity()
        {
            Id = Guid.Parse(product.Store.Id),
            Name = product.Store.Name,
            Picture = product.Store.Picture
        };
        return prdEntityData;
    }
}
