﻿using FrameworkCore.Domains.Cart.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Cart.Infrastructure.Service.Store;

public interface IProductUsecase
{
    public Task<ProductEntity?> GetProductById(int id);
    public Task<Dictionary<int, ProductEntity>> GetListProductById(int[] ids);
}
