﻿using FrameworkCore.Domains.Cart.Entities;
using FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext;

namespace FrameworkCore.Domains.Cart.Infrastructure.Service.User;

public class UserUsecaseSqlServerImpl : IUserUsecase
{
    EshopMsSqlDbContext _dbCtx;
    public UserUsecaseSqlServerImpl(EshopMsSqlDbContext dbCtx)
    {
        _dbCtx = dbCtx;
    }

    public async Task<UserEntity?> FindUserById(string id)
    {
        var userData = _dbCtx.AppUsers.Where(usr => usr.Id.Equals(id)).FirstOrDefault();
        if (userData == null) return null;

        var result = new UserEntity()
        {
            Id = userData.Id,
            Name = userData.GetDisplayName(),
            Email = userData.GoogleAccountEmail,
            Picture = userData.GoogleAccountPicture
        };

        return await Task.FromResult(result);
    }
}
