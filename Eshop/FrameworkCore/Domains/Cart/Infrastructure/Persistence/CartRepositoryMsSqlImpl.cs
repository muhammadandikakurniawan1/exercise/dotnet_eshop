﻿using FrameworkCore.Domains.Cart.Aggregates.Cart;
using FrameworkCore.Domains.Cart.Entities;
using FrameworkCore.Domains.Cart.Infrastructure.Persistence.TableModels;
using FrameworkCore.Domains.Cart.Repositories;
using FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Xunit;

namespace FrameworkCore.Domains.Cart.Infrastructure.Persistence
{
    public class CartRepositoryMsSqlImpl : ICartRepository
    {
        private EshopMsSqlDbContext _dbCtx;
        public CartRepositoryMsSqlImpl(EshopMsSqlDbContext dbCtx)
        {
            _dbCtx = dbCtx;
        }
        public async Task<CartAggregate> SaveAsync(CartAggregate aggregateData)
        {
            var listAggregateEvent = aggregateData.GetEvents();

            using (var scope = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            {
                try
                {
                    foreach (var e in listAggregateEvent)
                    {
                        if (e is AddItemEvent)
                        {
                            var aggEvent = e as AddItemEvent;
                            var handlerRes = await EventHandler(aggEvent!);
                        }
                        else if (e is UpdateItemQtyEvent)
                        {
                            var aggEvent = e as UpdateItemQtyEvent;
                            var handlerRes = await EventHandler(aggEvent!);
                        }
                    }

                    scope.Complete();
                    scope.Dispose();
                }
                catch (Exception)
                {
                    scope.Dispose();
                    throw;
                }
            }

            return await Task.FromResult(aggregateData);
        }

        public CartAggregate FindByUserIdAndProductId(string userId, int productId)
        {
            var cartData = new CartEntity();
            cartData.UserId = userId;
            var result = new CartAggregate(cartData);
            var cartItem = _dbCtx.CartItems
            .Where(c => c.UserId == userId && c.ProductId == productId)
            .FirstOrDefault();
            if (cartItem == null) return result;

            
            cartData.Items.Add(cartItem.ToEntity());
            result = new CartAggregate(cartData);
            return result;
        }

        public async Task<CartItem> EventHandler(AddItemEvent e)
        {
            var eventData = e.GetEventData();
            var cartItemData = new CartItem(eventData);
            var addRes = await _dbCtx.CartItems.AddAsync(cartItemData);
            cartItemData = addRes.Entity;
            await _dbCtx.SaveChangesAsync();
            return cartItemData;
        }

        public async Task<CartItem> EventHandler(UpdateItemQtyEvent e)
        {
            var eventData = e.GetEventData();
            var cartItemData = new CartItem(eventData);

            //var rowsModified = context.Database.ExecuteSql($"UPDATE [Blogs] SET [Url] = NULL");
            var userId = new SqlParameter("userId", eventData.UserId);
            var productId = new SqlParameter("productId", eventData.Product.Id);
            var cartItemQty = new SqlParameter("qty", eventData.Qty);
            var updatetedAt = new SqlParameter("updatedAt", eventData.UpdatedAt);
            var updateRes = _dbCtx.Database.ExecuteSqlRaw("UPDATE cart_item SET qty = @qty, updated_at = @updatedAt WHERE user_id = @userId AND product_id = @productId", cartItemQty, updatetedAt, userId, productId);
            if (updateRes <= 0)
            {
                throw new Exception("update failed");
            }
            await _dbCtx.SaveChangesAsync();
            return cartItemData;
        }

        public CartAggregate FindByUserId(string userId)
        {
            var cartEntity = new CartEntity();
            cartEntity.Items = _dbCtx.CartItems.Where(c => c.UserId.Equals(userId))
            .OrderByDescending(p => p.UserId)
            .OrderByDescending(p => p.CreatedAt)
            .Select(prd => prd.ToEntity()).ToList();

            return new CartAggregate(cartEntity);
        }
    }
}
