﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FrameworkCore.Domains.Cart.Entities;
using Microsoft.EntityFrameworkCore;

namespace FrameworkCore.Domains.Cart.Infrastructure.Persistence.TableModels;

[Table("cart_item")]
[Index("ProductId", Name = "product_id_idx")]
[Index("UserId", Name = "user_id_idx")]
public partial class CartItem
{
    [Key]
    [Column("id")]
    public int Id { get; set; }

    [Column("user_id")]
    [StringLength(255)]
    [Unicode(false)]
    public string UserId { get; set; } = null!;

    [Column("product_id")]
    public int ProductId { get; set; }

    [Column("qty")]
    public int Qty { get; set; }

    [Column("store_id")]
    [StringLength(255)]
    [Unicode(false)]
    public string StoreId { get; set; } = null!;

    [Column("created_at")]
    public long CreatedAt { get; set; }

    [Column("updated_at")]
    public long UpdatedAt { get; set; }

    [Column("deleted_at")]
    public long? DeletedAt { get; set; }

    public CartItem() { }


    public CartItem(CartItemEntity entityData)
    {
        this.Id = entityData.Id;
        this.StoreId = entityData.Store.Id.ToString() ?? "";
        this.UserId = entityData.UserId;
        this.ProductId = entityData.Product.Id;
        this.Qty = entityData.Qty;
        this.CreatedAt = entityData.CreatedAt;
        this.UpdatedAt = entityData.UpdatedAt;
        this.DeletedAt = entityData.DeletedAt;
    }

    public CartItemEntity ToEntity()
    {
        var result = new CartItemEntity();

        result.Id = this.Id;
        result.Qty = this.Qty;
        result.UserId = this.UserId;
        result.Product = new ProductEntity()
        {
            Id = this.ProductId,
        };

        result.Store = new StoreEntity()
        {
            Id = Guid.Parse(this.StoreId)
        };

        result.CreatedAt = this.CreatedAt;
        result.UpdatedAt = this.UpdatedAt;
        result.DeletedAt = this.DeletedAt;

        return result;
    }
}
