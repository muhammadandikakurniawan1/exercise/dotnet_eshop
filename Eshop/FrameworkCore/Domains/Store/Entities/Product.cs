﻿using FrameworkCore.Domains.Store.ValueObjects;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FrameworkCore.Domains.Store.Entities
{
    public class ProductEntity : BaseEntity
    {
        public int Id { get; set; } = 0;
        public Guid StoreId { get; set; }
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public int Stock { get; set; } = 0;
        public StoreEntity StoreData { set; get; } = new StoreEntity();
        public List<ProductPrice> Prices { get; set; } = new List<ProductPrice>();
        public ProductPrice Price { get; set; } = new ProductPrice();
        public List<ProductImage> Pictures { get; set; } = new List<ProductImage>();
        public List<IFormFile> PictureFiles { get; set; }

        public bool NameIsEmpty()
        {
            return string.IsNullOrEmpty(Name) || string.IsNullOrWhiteSpace(Name);
        }

        public bool DescriptionIsEmpty()
        {
            return string.IsNullOrEmpty(Description) || string.IsNullOrWhiteSpace(Description);
        }

        public bool PicturesIsEmpty()
        {
            return Pictures.Count <= 0;
        }

        public bool PricesIsEmpty()
        {
            return Prices.Count <= 0;
        }

        public List<string> Validate()
        {
            var errorMessages = new List<string>();
            if (this == null) errorMessages.Add("new product can't be empty");
            if (this.NameIsEmpty()) errorMessages.Add("product name can't be empty");
            if (this.DescriptionIsEmpty()) errorMessages.Add("product description can't be empty");
            if (this.PricesIsEmpty()) errorMessages.Add("product price can't be empty");
            if (this.PicturesIsEmpty()) errorMessages.Add("product pictures can't be empty");

            return errorMessages;
        }
    }
}
