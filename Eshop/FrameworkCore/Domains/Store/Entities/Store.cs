﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Entities
{
    public class StoreEntity : BaseEntity
    {
        public Guid? Id { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; } = "";
        public string Picture { get; set; } = "";
        public string HeaderPicture { get; set; } = "";
        public List<ProductEntity> ListProduct { get; set; } = new List<ProductEntity>();
        public UserEntity Owner = new UserEntity();

        public bool NameIsEmpty()
        {
            return string.IsNullOrEmpty(Name) || string.IsNullOrWhiteSpace(Name);
        }
    }
}
