﻿using FrameworkCore.Domains.Store.Aggregates.Store;
using FrameworkCore.Domains.Store.Repositories;
using FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace FrameworkCore.Domains.Store.Infrastructure.Persistence
{
    public class StoreRepositoryMsSqlImpl : IStoreRepository
    {
        private EshopMsSqlDbContext _dbCtx;
        public StoreRepositoryMsSqlImpl(EshopMsSqlDbContext dbCtx)
        {
            _dbCtx = dbCtx;
        }
        public async Task<StoreAggregate> SaveAsync(StoreAggregate aggregateData)
        {
            var listAggregateEvent = aggregateData.GetEvents();

            using (var scope = new TransactionScope(TransactionScopeOption.Required, TransactionScopeAsyncFlowOption.Enabled))
            {

                try
                {
                    foreach (var e in listAggregateEvent)
                    {
                        if (e is CreateNewStoreEvent)
                        {
                            var aggEvent = e as CreateNewStoreEvent;
                            var handlerRes = EventHandler(aggEvent!);
                        }else if (e is CreateNewProductEvent){
                            var aggEvent = e as CreateNewProductEvent;
                            var handlerRes = EventHandler(aggEvent!);
                            aggregateData.GetStore().ListProduct[0] = handlerRes.ToEntity();
                            Console.WriteLine(aggregateData);
                        }
                    }

                    scope.Complete();
                    scope.Dispose();
                }
                catch (Exception)
                {
                    scope.Dispose();
                    throw;
                }
            }

            return await Task.FromResult(aggregateData);
        }
        public StoreAggregate? FindByID(string id)
        {
            var storeData = _dbCtx.Stores.Where(s => s.Id.Equals(id)).FirstOrDefault();
            if (storeData == null) return null;

            var aggreagateData = new StoreAggregate(storeData.ToEntity());
            return aggreagateData;
        }

        public StoreAggregate? FindByUserID(string id)
        {
            var storeData = _dbCtx.Stores.Where(s => s.UserId.Equals(id)).FirstOrDefault();
            if (storeData == null) return null;

            var aggreagateData = new StoreAggregate(storeData.ToEntity());
            return aggreagateData;
        }

        public TableModels.Store EventHandler(CreateNewStoreEvent aggEvent)
        {
            var eventData = aggEvent.GetEventData();
            var createRes = _dbCtx.Stores.Add(new TableModels.Store(eventData));
            _dbCtx.SaveChanges();
            return createRes.Entity;
        }

        public TableModels.Product EventHandler(CreateNewProductEvent aggEvent)
        {
            var eventData = aggEvent.GetEventData();

            // insert product
            var productData = new TableModels.Product(eventData);
            productData = _dbCtx.Products.Add(productData).Entity;
            _dbCtx.SaveChanges();


            // insert product price
            var priceProductData = new TableModels.ProductPrice(eventData.Prices.FirstOrDefault()!);
            priceProductData.ProductId = productData.Id;
            priceProductData = _dbCtx.ProductPrices.Add(priceProductData).Entity;
            _dbCtx.SaveChanges();
            productData.ProductPrices.Add(priceProductData);

            var productImagesData = eventData.Pictures.Select(p => new TableModels.ProductImage(productId: productData.Id, url: p.Url)).ToList();
            _dbCtx.ProductImages.AddRange(productImagesData);
            _dbCtx.SaveChanges();
            productImagesData.ForEach(e => productData.ProductImages.Add(e));
            

            return productData;
        }
    }
}
