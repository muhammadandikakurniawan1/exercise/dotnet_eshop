﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace FrameworkCore.Domains.Store.Infrastructure.Persistence.TableModels;

[Table("product_image")]
[Index("Url", Name = "product_image_url_idx")]
public partial class ProductImage
{
    [Key]
    [Column("id")]
    public int Id { get; set; }

    [Column("product_id")]
    public int ProductId { get; set; }

    [Column("url")]
    [StringLength(1000)]
    [Unicode(false)]
    public string Url { get; set; } = null!;

    [Column("created_at")]
    public long CreatedAt { get; set; }

    [Column("updated_at")]
    public long UpdatedAt { get; set; }

    [Column("deleted_at")]
    public long? DeletedAt { get; set; }

    [ForeignKey("ProductId")]
    [InverseProperty("ProductImages")]
    public virtual Product Product { get; set; } = null!;

    public ProductImage() { }
    public ProductImage( int productId, string url)
    {
        ProductId = productId;
        Url = url;
    }

    public FrameworkCore.Domains.Store.ValueObjects.ProductImage ToValueObject()
    {
        return new FrameworkCore.Domains.Store.ValueObjects.ProductImage()
        {
            ProductId = this.ProductId,
            Url = this.Url,
        };
    }
}
