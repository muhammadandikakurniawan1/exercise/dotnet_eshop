﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FrameworkCore.Domains.Store.Entities;
using Microsoft.EntityFrameworkCore;

namespace FrameworkCore.Domains.Store.Infrastructure.Persistence.TableModels;

[Table("store")]
[Index("Name", Name = "store_name_idx")]
public partial class Store
{
    [Key]
    [Column("id")]
    [StringLength(255)]
    [Unicode(false)]
    public string Id { get; set; } = null!;

    [Column("name")]
    [StringLength(255)]
    [Unicode(false)]
    public string Name { get; set; } = null!;

    [Column("user_id")]
    [StringLength(255)]
    [Unicode(false)]
    public string UserId { get; set; } = null!;

    [Column("picture", TypeName = "text")]
    public string Picture { get; set; } = null!;

    [Column("header_picture", TypeName = "text")]
    public string HeaderPicture { get; set; } = null!;

    [Column("created_at")]
    public long CreatedAt { get; set; }

    [Column("updated_at")]
    public long UpdatedAt { get; set; }

    [Column("deleted_at")]
    public long? DeletedAt { get; set; }

    [InverseProperty("Store")]
    public virtual ICollection<Product> Products { get; } = new List<Product>();

    public Store() { }

    public Store(StoreEntity entityData)
    {
        this.Id = entityData.Id?.ToString() ?? "";
        this.UserId = entityData.UserId.ToString();
        this.Name = entityData.Name; 
        this.Picture = entityData.Picture;
        this.HeaderPicture = entityData.HeaderPicture;
        this.CreatedAt = entityData.CreatedAt;
        this.UpdatedAt = entityData.UpdatedAt;
        this.DeletedAt = entityData.DeletedAt;
    }

    public StoreEntity ToEntity(bool parseProduct = true)
    {
        var result = new StoreEntity()
        {
            Id = Guid.Parse(this.Id),
            UserId = Guid.Parse(this.UserId),
            Name = this.Name,
            Picture = this.Picture,
            HeaderPicture = this.HeaderPicture,
            CreatedAt = this.CreatedAt,
            UpdatedAt = this.UpdatedAt,
            DeletedAt = this.DeletedAt,
        };
        if (parseProduct) result.ListProduct = this.Products.ToList().Select(prd => prd.ToEntity()).ToList();

        return result;
    }
}
