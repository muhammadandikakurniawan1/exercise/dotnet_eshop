﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace FrameworkCore.Domains.Store.Infrastructure.Persistence.TableModels;

[Table("product_price")]
public partial class ProductPrice
{
    [Key]
    [Column("id")]
    public int Id { get; set; }

    [Column("product_id")]
    public int ProductId { get; set; }

    [Column("currency_code")]
    [StringLength(50)]
    [Unicode(false)]
    public string CurrencyCode { get; set; } = null!;

    [Column("value")]
    public double Value { get; set; }

    [Column("created_at")]
    public long CreatedAt { get; set; }

    [Column("updated_at")]
    public long UpdatedAt { get; set; }

    [Column("deleted_at")]
    public long? DeletedAt { get; set; }

    [ForeignKey("ProductId")]
    [InverseProperty("ProductPrices")]
    public virtual Product Product { get; set; } = null!;

    public ProductPrice()
    {

    }

    public ProductPrice(FrameworkCore.Domains.Store.ValueObjects.ProductPrice data)
    {
        this.ProductId = data.ProductId;
        this.CurrencyCode = data.Currency;
        this.Value = data.Value;
    }

    public FrameworkCore.Domains.Store.ValueObjects.ProductPrice ToValueObject()
    {
        return new FrameworkCore.Domains.Store.ValueObjects.ProductPrice()
        {
            ProductId = this.ProductId,
            Value = this.Value,
        };
    }
}
