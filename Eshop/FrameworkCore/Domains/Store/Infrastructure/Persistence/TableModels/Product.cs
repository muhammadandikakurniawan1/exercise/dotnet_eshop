﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FrameworkCore.Domains.Store.Entities;
using Microsoft.EntityFrameworkCore;

namespace FrameworkCore.Domains.Store.Infrastructure.Persistence.TableModels;

[Table("product")]
[Index("Name", Name = "store_name_idx")]
public partial class Product
{
    [Key]
    [Column("id")]
    public int Id { get; set; }

    [Column("store_id")]
    [StringLength(255)]
    [Unicode(false)]
    public string StoreId { get; set; } = null!;

    [Column("name")]
    [StringLength(255)]
    [Unicode(false)]
    public string Name { get; set; } = null!;

    [Column("description", TypeName = "text")]
    public string Description { get; set; } = null!;

    [Column("stock")]
    public int Stock { get; set; }

    [Column("created_at")]
    public long CreatedAt { get; set; }

    [Column("updated_at")]
    public long UpdatedAt { get; set; }

    [Column("deleted_at")]
    public long? DeletedAt { get; set; }

    [InverseProperty("Product")]
    public virtual ICollection<ProductImage> ProductImages { get; } = new List<ProductImage>();

    [InverseProperty("Product")]
    public virtual ICollection<ProductPrice> ProductPrices { get; } = new List<ProductPrice>();

    [ForeignKey("StoreId")]
    [InverseProperty("Products")]
    public virtual Store Store { get; set; } = null!;

    public Product() { }
     
    public Product(ProductEntity data)
    {
        this.StoreId = data.StoreId.ToString();
        this.Name = data.Name;
        this.Description = data.Description;
        this.Stock = data.Stock;
        this.CreatedAt = data.CreatedAt;
        this.UpdatedAt = data.UpdatedAt;
        this.DeletedAt = data.DeletedAt;
    }

    public ProductEntity ToEntity()
    {
        var result = new ProductEntity()
        {
            Id = this.Id,
            Name = this.Name,
            Description = this.Description,
            Prices = this.ProductPrices.ToList().Select(prc => prc.ToValueObject()).ToList(),
            Pictures = this.ProductImages.ToList().Select(img => img.ToValueObject()).ToList(),
            StoreId = Guid.Parse(this.StoreId),
            StoreData = this.Store.ToEntity(false),
            CreatedAt = this.CreatedAt,
            UpdatedAt = this.UpdatedAt,
            DeletedAt = this.DeletedAt,
        };


        return result;
    }
}
