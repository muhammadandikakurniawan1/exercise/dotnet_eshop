﻿using FrameworkCore.Domains.Store.Aggregates.Store;
using FrameworkCore.Domains.Store.Entities;
using FrameworkCore.Domains.Store.Repositories;
using FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using FrameworkCore.Domains.Store.Aggregates.Product;

namespace FrameworkCore.Domains.Store.Infrastructure.Persistence
{
    public class ProductRepositoryMsSqlImpl : IProductRepository
    {
        private EshopMsSqlDbContext _dbCtx;
        public ProductRepositoryMsSqlImpl(EshopMsSqlDbContext dbCtx)
        {
            _dbCtx = dbCtx;
        }

        public ProductAggregate? FindByID(int id)
        {
            var productData = _dbCtx.Products
            .Where(prd => prd.Id == id)
            .Include(prd => prd.ProductImages)
            .Include(prd => prd.ProductPrices)
            .Include(prd => prd.Store)
            .FirstOrDefault();
            if (productData == null) return null;

            return new ProductAggregate(productData.ToEntity());
        }

        public async Task<List<ProductAggregate>> FindPagination(int skip, int take, string searchKey)
        {
            var isSearchKeywordExists = !string.IsNullOrEmpty(searchKey) && !string.IsNullOrWhiteSpace(searchKey);
            var listProductAggregate = await _dbCtx.Products.AsQueryable()
            .Where(prd => (isSearchKeywordExists ? prd.Name.Contains(searchKey) : true) && prd.DeletedAt == null)
            .Skip(skip)
            .Take(take)
            .Include(prd => prd.ProductImages)
            .Include(prd => prd.ProductPrices)
            .Include(prd => prd.Store)
            .Select(prd => new ProductAggregate(prd.ToEntity()))
            .ToListAsync();

            return listProductAggregate;
        }

        public async Task<int> CountPagination(string searchKey)
        {
            var isSearchKeywordExists = !string.IsNullOrEmpty(searchKey) && !string.IsNullOrWhiteSpace(searchKey);
            var count = await _dbCtx.Products.AsQueryable().Where(prd => (isSearchKeywordExists ? prd.Name.Contains(searchKey) : true) && prd.DeletedAt == null).CountAsync();
            return count;
        }
    }
}
