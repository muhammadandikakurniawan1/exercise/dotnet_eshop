﻿using FrameworkCore.Domains.Store.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Infrastructure.Service.User
{
    public interface IUserUsecase
    {
        public Task<UserEntity?> FindUserById(string id);
    }
}

