﻿using FrameworkCore.Domains.Store.Entities;
using FrameworkCore.Primitive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Aggregates.Store
{
    public class StoreAggregate : Aggregate
    {
        private StoreEntity storeData = new StoreEntity();
        public StoreAggregate(StoreEntity storeData)
        {
            this.storeData = storeData;
        }
        public StoreAggregate()
        {

        }

        public StoreEntity GetStore()
        {
            return storeData;
        }

        public void CreateNewStore()
        {
            if (storeData.NameIsEmpty()) throw new ArgumentNullException("store name can't be empty");

            DateTime now = DateTime.UtcNow;
            long createdAt = new DateTimeOffset(now).ToUnixTimeMilliseconds();

            this.storeData.Id = Guid.NewGuid();
            this.storeData.CreatedAt = createdAt;
            this.storeData.UpdatedAt = createdAt;
            var createNewStoreEvent = new CreateNewStoreEvent(this.storeData);
            base.AddEvent(createNewStoreEvent);
        }

        public void AddNewProduct(ProductEntity newProduct)
        {

            long createdAt = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
            newProduct.CreatedAt = createdAt;
            newProduct.UpdatedAt = createdAt;

            newProduct.Prices = newProduct.Prices ?? new List<ValueObjects.ProductPrice>();
            newProduct.Prices.Add(newProduct.Price);

            var errorValidationMessages = newProduct.Validate();
            var invalidProduct = errorValidationMessages.Count > 0;
            if (invalidProduct) throw new ArgumentException(string.Join(',', errorValidationMessages));

            var createNewProduct = new CreateNewProductEvent(newProduct);
            base.AddEvent(createNewProduct);

            this.storeData.ListProduct.Add(newProduct);
        }
    }
}
