﻿using FrameworkCore.Domains.Store.Entities;
using FrameworkCore.Primitive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Aggregates.Store;

public class CreateNewStoreEvent : AggregateEvent<StoreEntity>
{
    public CreateNewStoreEvent(StoreEntity data) : base(data)
    {

    }
}

public class CreateNewProductEvent : AggregateEvent<ProductEntity>
{
    public CreateNewProductEvent(ProductEntity data) : base(data)
    {

    }
}
