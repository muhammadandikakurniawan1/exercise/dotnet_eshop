﻿using FrameworkCore.Domains.Store.Entities;
using FrameworkCore.Primitive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Aggregates.Product;

public class ProductAggregate : Aggregate
{
    private ProductEntity productData { get; set; }
    public ProductAggregate() { }
    public ProductAggregate(ProductEntity productData) {
        this.productData = productData;
    }

    public void Validate()
    {
        if (productData == null) throw new ArgumentNullException("new product can't be empty");
        if (productData.NameIsEmpty()) throw new ArgumentNullException("product name can't be empty");
        if (productData.DescriptionIsEmpty()) throw new ArgumentNullException("product description can't be empty");
        if (productData.PricesIsEmpty()) throw new ArgumentNullException("product price can't be empty");
        if (productData.PicturesIsEmpty()) throw new ArgumentNullException("product pictures can't be empty");
    }

    public ProductEntity GetProduct()
    {
        return productData;
    }
}