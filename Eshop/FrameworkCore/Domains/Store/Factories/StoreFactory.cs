﻿using FrameworkCore.Domains.Store.Aggregates.Store;
using FrameworkCore.Domains.Store.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Factories
{
    public static class StoreFactory
    {
        public static StoreAggregate BuildNewStore(StoreEntity entityData)
        {
            var aggregateData = new StoreAggregate(entityData);
            aggregateData.CreateNewStore();
            return aggregateData;
        }
    }
}
