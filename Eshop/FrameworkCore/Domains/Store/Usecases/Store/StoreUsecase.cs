﻿using FrameworkCore.Domains.Store.Entities;
using FrameworkCore.Domains.Store.Factories;
using FrameworkCore.Domains.Store.Infrastructure.Service.User;
using FrameworkCore.Domains.Store.Repositories;
using FrameworkCore.Domains.Store.Usecases.Store.Models;
using FrameworkCore.Domains.Store.ValueObjects;
using FrameworkCore.Pkg.FileStorageHelper;
using FrameworkCore.Pkg.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Usecases.Store;

public class StoreUsecase : IStoreUsecase
{
    private IConfigurationRoot _config;
    private IUserUsecase _userServiceUsecase;
    private IStoreRepository _storeRepository;
    private IFileStorageHelper _fileStorageHelper;
    private string _defaultStoreImage = "";
    private string _productImagePath = "";
    private string _storeImagePath = "";
    public StoreUsecase(
        IConfigurationRoot config,
        IStoreRepository storeRepository,
        IUserUsecase userServiceUsecase,
        IFileStorageHelper fileStorageHelper
    )
    {
        _storeRepository = storeRepository;
        _userServiceUsecase = userServiceUsecase;
        _fileStorageHelper = fileStorageHelper;
        _config = config;
        _defaultStoreImage = _config.GetSection("DefaultStoreImage").Value ?? "";
        _productImagePath = _config.GetSection("ProductImagePath").Value ?? "";
        _storeImagePath = _config.GetSection("StoreImagePath").Value ?? "";
    }


    public async Task<BaseResponseModel<StoreEntity>> GetMyStore(string userId)
    {
        var result = new BaseResponseModel<StoreEntity>();
        try
        {
            var storeData = _storeRepository.FindByUserID(userId);
            if(storeData == null)
            {
                result.Message = "data not found";
                result.StatusCode = "STORE204";
                result.HttpCode = System.Net.HttpStatusCode.NoContent;
                return await Task.FromResult(result);
            }

            result.Data = storeData.GetStore();
            result.IsSuccess = true;
            result.Message = "success";
            result.StatusCode = "STORE200";
            result.HttpCode = System.Net.HttpStatusCode.OK;
        }
        catch(Exception ex)
        {
            result.SetInternalServerError(ex);
        }

        return await Task.FromResult(result);
    }

    public async Task<BaseResponseModel<StoreEntity>> CreateStore(CreateNewStoreRequest request)
    {
        var result = new BaseResponseModel<StoreEntity>();
        try
        {

            var storeData = _storeRepository.FindByUserID(request.UserId);
            if(storeData != null)
            {
                result.Message = "store already exists";
                result.StatusCode = "STORE400";
                result.HttpCode = System.Net.HttpStatusCode.BadRequest;
                return await Task.FromResult(result);
            }

            var createdAt = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();

            string headerPict = request.HeaderPictIsEmpty() ? _defaultStoreImage : "";
            string profilePict = request.ProfilePictIsEmpty() ? _defaultStoreImage : "";

            await Task.WhenAll(new Task[] {
                Task.Run(async () => {
                    if(headerPict.Equals(""))
                    {
                        var headerPictName = $"{request.UserId}{createdAt+1}";
                        var headerPictBase64 = imageBase64Filter(request.HeaderPictBase64, headerPictName);
                        headerPict = await uploadImageAsync(headerPictBase64);
                    }
                }),
                Task.Run(async () => {
                    if(profilePict.Equals(""))
                    {
                        var profilePictName = $"{request.UserId}{createdAt+2}";
                        var profilePictBase64 = imageBase64Filter(request.ProfilePictBase64, profilePictName);
                        profilePict = await uploadImageAsync(profilePictBase64);
                    }
                })
            });

            var storeEntityData = new StoreEntity()
            {
                UserId = Guid.Parse(request.UserId),
                HeaderPicture = headerPict,
                Picture = profilePict,
                Name = request.StoreName
            };
            
            var newStoreData = StoreFactory.BuildNewStore(storeEntityData);

            newStoreData = await _storeRepository.SaveAsync(newStoreData);
            result.Data = newStoreData.GetStore();
            result.IsSuccess = true;
            result.Message = "success";
            result.StatusCode = "STORE200";
            result.HttpCode = System.Net.HttpStatusCode.OK;
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex);
            result.SetInternalServerError(ex);
        }

        return result;
    }

    public async Task<BaseResponseModel<ProductEntity>> CreateNewProduct(ProductEntity newProduct)
    {
        var result = new BaseResponseModel<ProductEntity>();
        try
        {
            var storeData = _storeRepository.FindByID(newProduct.StoreId.ToString());
            if (storeData == null)
            {
                result.Message = "data not found";
                result.StatusCode = "STORE204";
                result.HttpCode = System.Net.HttpStatusCode.NoContent;
                return result;
            }

            var pictureLocations = await uploadProductPictures(newProduct.PictureFiles!, newProduct.StoreId.ToString()!) ?? new string[] {};

            if (newProduct.Pictures == null) newProduct.Pictures = new List<ProductImage>();
            newProduct.Pictures = pictureLocations.Select(p => new ProductImage(url : p)).ToList();

            storeData.AddNewProduct(newProduct);
            storeData = await _storeRepository.SaveAsync(storeData);

            newProduct = storeData.GetStore().ListProduct.FirstOrDefault()!;
            result.Data = newProduct;
            result.IsSuccess = true;
            result.Message = "success";
            result.StatusCode = "STORE200";
            result.HttpCode = System.Net.HttpStatusCode.OK;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
            result.SetInternalServerError(ex);
        }
        return result;
    }

    private async Task<string[]> uploadProductPictures(List<IFormFile> pictureFiles, string storeId)
    {
        var result = new string[pictureFiles.Count];
        if (pictureFiles == null) return await Task.FromResult(result);

        var uploadFileTasks = new List<Task>();

        for (var idx = 0; idx < pictureFiles.Count; idx++)
        {
            var formFile = pictureFiles[idx];
            var uploadFileTask = async Task () =>
            {
                result[idx] = await UploadProductPictureFormFile(formFile, storeId, idx);
            };

            uploadFileTasks.Add(uploadFileTask());
        }

        await Task.WhenAll(uploadFileTasks);

        return result;
    }

    private async Task<string> UploadProductPictureFormFile(IFormFile file, string storeID, int idx)
    {

        string bucketName, filePath;
        setProductImageBucketAndFilePath(out bucketName, out filePath);

        var createdAt = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
        var filename = $"{filePath}/{createdAt}-{storeID}-{idx}-{file.FileName}";
        var uploadOpt = new SaveFileOpt()
        {
            FileStream = file.OpenReadStream(),
            FileName = filename,
            ContentType = file.ContentType,
            Path = bucketName
        };

        return await uploadImageAsync(uploadOpt);
    }

    private void setProductImageBucketAndFilePath(out string bucketName, out string filenamePath)
    {
        bucketName = _productImagePath ?? "";
        filenamePath = "";
        var bucketChunk = bucketName.Split("/");
        if (bucketChunk.Length >= 2)
        {
            bucketName = bucketChunk[0];
            filenamePath = string.Join('/', bucketChunk.Skip(1));
        }
    }

    private SaveFileOpt imageBase64Filter(string strBase64, string filename)
    {
        var base64Str = strBase64;
        var contentType = "";
        var fieExt = "jpg";
        var base64Chunk = strBase64.Split(";base64,");
        if(base64Chunk.Length >= 2)
        {
            base64Str = base64Chunk[1];
            contentType = base64Chunk[0].Replace("data:", "");
        }

        var contentTypeChunk = contentType.Split('/');
        if(contentTypeChunk.Length >= 2)
        {
            fieExt = contentTypeChunk[1];
        }

        var bytesData = System.Convert.FromBase64String(base64Str);
        MemoryStream ms = new MemoryStream(bytesData);

        var bucketName = _config.GetSection("StoreImagePath").Value ?? "";

        var bucketChunk = bucketName.Split("/");
        if(bucketChunk.Length >= 2)
        {
            bucketName = bucketChunk[0];
            var filenamePath =  string.Join('/', bucketChunk.Skip(1));
            filename = $"{filenamePath}/{filename}.{fieExt}";
        }


        var result = new SaveFileOpt()
        {
            FileByte = bytesData,
            FileStream = ms,
            FileName = filename,
            ContentType = contentType,
            Path = bucketName,
        };

        return result;
    }

    private async Task<string> uploadImageAsync(SaveFileOpt opt)
    {
        var result = await _fileStorageHelper.SaveAsync(opt);
        return result.FileLocation;
    }
}
