﻿using FrameworkCore.Domains.Store.Entities;
using FrameworkCore.Domains.Store.Usecases.Store.Models;
using FrameworkCore.Pkg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Usecases.Store;

public interface IStoreUsecase {
    public Task<BaseResponseModel<StoreEntity>> GetMyStore(string userId);
    public Task<BaseResponseModel<StoreEntity>> CreateStore(CreateNewStoreRequest request);
    public Task<BaseResponseModel<ProductEntity>> CreateNewProduct(ProductEntity newProduct);
}
