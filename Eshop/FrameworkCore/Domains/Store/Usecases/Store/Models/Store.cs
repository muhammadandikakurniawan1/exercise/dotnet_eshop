﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FrameworkCore.Domains.Store.Usecases.Store.Models;

public class CreateNewStoreRequest
{
    public string UserId { get; set; } = "";
    public string StoreName { get; set; } = "";
    public string HeaderPictBase64 { get; set; } = "";
    public string ProfilePictBase64 { get; set; } = "";

    public bool HeaderPictIsEmpty()
    {
        return string.IsNullOrEmpty(HeaderPictBase64) || string.IsNullOrWhiteSpace(HeaderPictBase64);
    }
    public bool ProfilePictIsEmpty()
    {
        return string.IsNullOrEmpty(ProfilePictBase64) || string.IsNullOrWhiteSpace(ProfilePictBase64);
    }
}
