﻿using FrameworkCore.Domains.Store.Usecases.Product.Models;
using FrameworkCore.Pkg.Models;
using FrameworkCore.Domains.Store.Infrastructure.Service.User;
using FrameworkCore.Domains.Store.Repositories;
using FrameworkCore.Pkg.FileStorageHelper;
using Microsoft.Extensions.Configuration;
using FrameworkCore.Domains.Store.Entities;
using FrameworkCore.Domains.Store.Aggregates.Product;

namespace FrameworkCore.Domains.Store.Usecases.Product;

public class ProductUsecase : IProductUsecase
{
    private IConfigurationRoot _config;
    private IUserUsecase _userServiceUsecase;
    private IStoreRepository _storeRepository;
    private IProductRepository _productRepository;
    private IFileStorageHelper _fileStorageHelper;
    private string _defaultStoreImage = "";
    private int _defaultProductLength = 0;
    public ProductUsecase(
        IConfigurationRoot config,
        IStoreRepository storeRepository,
        IUserUsecase userServiceUsecase,
        IFileStorageHelper fileStorageHelper,
        IProductRepository productRepository
    )
    {
        _storeRepository = storeRepository;
        _userServiceUsecase = userServiceUsecase;
        _fileStorageHelper = fileStorageHelper;
        _config = config;
        _defaultStoreImage = _config.GetSection("DefaultStoreImage").Value ?? "";
        _defaultProductLength = int.Parse(_config.GetSection("DefaultProductLength").Value ?? "0");
        _productRepository = productRepository;
    }

    public async Task<BaseResponseModel<ProductPaginationModel>> GetProductPagination(ProductPaginationModel request)
    {
        var result = new BaseResponseModel<ProductPaginationModel>();

        var dataLength = request.DataCount <= 0 ? _defaultProductLength : request.DataCount;
        var currentPage = request.CurrentPage <= 0 ? 1 : request.CurrentPage;
        var dataSkip = (currentPage - 1) * dataLength;

        var searchKeyword = request.SearchKeyword ?? "";

        var listProductAggregate = new List<ProductAggregate>();
        var dataCount = 0;

        listProductAggregate = await _productRepository.FindPagination(dataSkip, dataLength, searchKeyword);
        dataCount = await _productRepository.CountPagination(searchKeyword);
        //await Task.WhenAll(new Task[]{
        //    Task.Run(async () => {
        //        listProductAggregate = await _productRepository.FindPagination(dataSkip, dataLength, searchKeyword);
        //    }),
        //    Task.Run(async () => {
        //        dataCount = await _productRepository.CountPagination(searchKeyword);
        //    })
        //});

        result.Data = new ProductPaginationModel()
        {
            ListProduct = listProductAggregate.Select(p => p.GetProduct()).ToList(),
            DataCount = dataCount,
            SearchKeyword = searchKeyword,
            CurrentPage = request.CurrentPage
        };
        return await Task.FromResult(result);
    }

    public async Task<BaseResponseModel<ProductEntity>> GetProductDetail(int productId)
    {
        var result = new BaseResponseModel<ProductEntity>();
        var productData = _productRepository.FindByID(productId);
        if(productData == null)
        {
            result.Message = "failed";
            result.StatusCode = "PRD204";
            result.HttpCode = System.Net.HttpStatusCode.NoContent;
            return await Task.FromResult(result);
        }

        result.Data = productData.GetProduct();
        result.IsSuccess = true;
        result.Message = "success";
        result.StatusCode = "PRD200";
        result.HttpCode = System.Net.HttpStatusCode.OK;
        return await Task.FromResult(result);
    }
}
