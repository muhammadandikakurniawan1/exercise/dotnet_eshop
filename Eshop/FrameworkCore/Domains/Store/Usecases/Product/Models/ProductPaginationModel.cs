﻿using FrameworkCore.Domains.Store.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Usecases.Product.Models
{
    public class ProductPaginationModel
    {
        public List<ProductEntity> ListProduct { get; set; } = new List<ProductEntity>();
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int DataCount { get; set; }
        public string SearchKeyword { get; set; } = "";
    }
}
