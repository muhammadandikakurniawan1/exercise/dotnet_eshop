﻿using FrameworkCore.Domains.Store.Entities;
using FrameworkCore.Domains.Store.Usecases.Product.Models;
using FrameworkCore.Pkg.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Usecases.Product;

public interface IProductUsecase
{
    public Task<BaseResponseModel<ProductPaginationModel>> GetProductPagination(ProductPaginationModel request);
    //public Task<BaseResponseModel<ProductPaginationModel>> CreateNewProduct();
    public Task<BaseResponseModel<ProductEntity>> GetProductDetail(int productId);
}
