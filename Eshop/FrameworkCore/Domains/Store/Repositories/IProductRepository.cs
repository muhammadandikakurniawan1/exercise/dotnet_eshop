﻿using FrameworkCore.Domains.Store.Aggregates.Product;
using FrameworkCore.Domains.Store.Aggregates.Store;
using FrameworkCore.Domains.Store.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Repositories
{
    public interface IProductRepository
    {
        public ProductAggregate? FindByID(int id);
        public Task<List<ProductAggregate>> FindPagination(int skip, int take, string searchKey);
        public Task<int> CountPagination(string searchKey);
    }
}
