﻿using FrameworkCore.Domains.Store.Aggregates.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.Repositories
{
    public interface IStoreRepository
    {
        public Task<StoreAggregate> SaveAsync(StoreAggregate agg);
        public StoreAggregate? FindByID(string id);
        public StoreAggregate? FindByUserID(string id);
    }
}
