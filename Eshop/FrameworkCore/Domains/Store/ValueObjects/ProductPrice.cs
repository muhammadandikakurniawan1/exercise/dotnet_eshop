﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.ValueObjects;

public class ProductPrice
{
    public int ProductId { get; set; }
    public string Currency { get; set; } = "";
    public double Value { get; set; } = 0;

    public ProductPrice() { }
    public ProductPrice(int productId, string currency, double value)
    {
        ProductId = productId;
        Currency = currency;
        Value = value;
    }
}