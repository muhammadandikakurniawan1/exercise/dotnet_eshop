﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Domains.Store.ValueObjects;

public class ProductImage
{
    public int ProductId { get; set; }
    public string Url { get; set; } = "";
    public string Base64 { get; set; } = "";

    public ProductImage() { }
    public ProductImage(string url, int productId = 0, string base64 = "")
    {
        ProductId = productId;
        Url = url;
        Base64 = base64;
    }
}