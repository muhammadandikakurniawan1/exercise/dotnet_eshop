﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Primitive
{
    public abstract class Aggregate
    {
        protected List<IAggregateEvent> _aggregateEvents = new List<IAggregateEvent>();
        protected void AddEvent(IAggregateEvent newEvent){
            _aggregateEvents.Add(newEvent);
        }

        public List<IAggregateEvent> GetEvents()
        {
            return this._aggregateEvents;
        }
    }

    public interface IAggregateEvent
    {
        public Object GetDataObject();
    }

    public abstract class AggregateEvent<T_DATA> : IAggregateEvent
    {
        protected T_DATA Data { get; set; }

        public AggregateEvent(T_DATA data)
        {
            if(data == null)
            {
                throw new ArgumentNullException();
            }
            Data = data;
        }

        public Object GetDataObject()
        {
            return (Object) this.Data;
        }

        public T_DATA GetEventData()
        {
            return Data;
        }
    }
}
