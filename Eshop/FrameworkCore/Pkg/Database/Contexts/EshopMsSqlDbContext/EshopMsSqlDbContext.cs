﻿using System;
using System.Collections.Generic;
using FrameworkCore.Domains.Cart.Infrastructure.Persistence.TableModels;
using FrameworkCore.Domains.Store.Infrastructure.Persistence.TableModels;
using FrameworkCore.Domains.User.Infrastructure.Persistence.TableModels;
using Microsoft.EntityFrameworkCore;

namespace FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext;

public partial class EshopMsSqlDbContext : DbContext
{
    public EshopMsSqlDbContext()
    {
    }

    public EshopMsSqlDbContext(DbContextOptions<EshopMsSqlDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<AppUser> AppUsers { get; set; }

    public virtual DbSet<CartItem> CartItems { get; set; }

    public virtual DbSet<Product> Products { get; set; }

    public virtual DbSet<ProductImage> ProductImages { get; set; }

    public virtual DbSet<ProductPrice> ProductPrices { get; set; }

    public virtual DbSet<Store> Stores { get; set; }

//    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//        => optionsBuilder.UseSqlServer("Server=127.0.0.1,1436;Database=eshop; User Id=sa; Password=P@ss0rdR00t@admin; Trusted_Connection=false; MultipleActiveResultSets=true;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<AppUser>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__app_user__3213E83F5E5FA06F");

            entity.Property(e => e.CreatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");
            entity.Property(e => e.Email).HasDefaultValueSql("('')");
            entity.Property(e => e.FullName).HasDefaultValueSql("('')");
            entity.Property(e => e.GoogleAccountEmail).HasDefaultValueSql("('')");
            entity.Property(e => e.GoogleAccountId).HasDefaultValueSql("('')");
            entity.Property(e => e.GoogleAccountName).HasDefaultValueSql("('')");
            entity.Property(e => e.GoogleAccountPicture).HasDefaultValueSql("('')");
            entity.Property(e => e.UpdatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");
        });

        modelBuilder.Entity<CartItem>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__cart_ite__3213E83F1B7D780E");

            entity.Property(e => e.CreatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");
            entity.Property(e => e.UpdatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");
            entity.Property(e => e.UserId).HasDefaultValueSql("('')");
        });

        modelBuilder.Entity<Product>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__product__3213E83F49AEF827");

            entity.Property(e => e.CreatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");
            entity.Property(e => e.Name).HasDefaultValueSql("('')");
            entity.Property(e => e.StoreId).HasDefaultValueSql("('')");
            entity.Property(e => e.UpdatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");

            entity.HasOne(d => d.Store).WithMany(p => p.Products)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__product__store_i__5BAD9CC8");
        });

        modelBuilder.Entity<ProductImage>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__product___3213E83FE24852A8");

            entity.Property(e => e.CreatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");
            entity.Property(e => e.UpdatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");
            entity.Property(e => e.Url).HasDefaultValueSql("('')");

            entity.HasOne(d => d.Product).WithMany(p => p.ProductImages)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__product_i__produ__690797E6");
        });

        modelBuilder.Entity<ProductPrice>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__product___3213E83F91F0A700");

            entity.Property(e => e.CreatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");
            entity.Property(e => e.CurrencyCode).HasDefaultValueSql("('RP')");
            entity.Property(e => e.UpdatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");

            entity.HasOne(d => d.Product).WithMany(p => p.ProductPrices)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__product_p__produ__625A9A57");
        });

        modelBuilder.Entity<Store>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PK__store__3213E83F4A3D3554");

            entity.Property(e => e.CreatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");
            entity.Property(e => e.Name).HasDefaultValueSql("('')");
            entity.Property(e => e.UpdatedAt).HasDefaultValueSql("(datediff_big(millisecond,'1970-01-01 00:00:00.000',sysutcdatetime()))");
            entity.Property(e => e.UserId).HasDefaultValueSql("('')");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
