﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext.Migrations
{
    /// <inheritdoc />
    public partial class CreateCartTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("CREATE TABLE cart_item (" +
                "id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1)," +
                "user_id varchar(255) NOT NULL DEFAULT '', " +
                "product_id INTEGER NOT NULL, " +
                "qty INTEGER NOT NULL DEFAULT 0, " +
                "created_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
                "updated_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
                "deleted_at BIGINT DEFAULT NULL, " +
                "INDEX user_id_idx(user_id), " +
                "INDEX product_id_idx(product_id)" +
            ")");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "cart_item");
        }
    }
}
