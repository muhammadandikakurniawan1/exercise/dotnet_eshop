﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext.Migrations
{
    /// <inheritdoc />
    public partial class CreateProductTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("CREATE TABLE product (" +
                "id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1)," +
                "store_id varchar(255) NOT NULL DEFAULT '', " +
                "name varchar(255) NOT NULL DEFAULT '', " +
                "description TEXT NOT NULL, " +
                "created_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
                "updated_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
                "deleted_at BIGINT DEFAULT NULL, " +
                "INDEX store_name_idx(name), " +
                "FOREIGN KEY (store_id) REFERENCES store(id)" +
            ")");

            migrationBuilder.Sql("CREATE TABLE product_price (" +
                "id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1)," +
                "product_id INTEGER NOT NULL, " +
                "currency_code varchar(50) NOT NULL DEFAULT 'RP', " +
                "value FLOAT NOT NULL, " +
                "created_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
                "updated_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
                "deleted_at BIGINT DEFAULT NULL, " +
                "FOREIGN KEY (product_id) REFERENCES product(id)" +
            ")");

            migrationBuilder.Sql("CREATE TABLE product_image (" +
                "id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1)," +
                "product_id INTEGER NOT NULL, " +
                "url varchar(1000) NOT NULL DEFAULT '', " +
                "created_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
                "updated_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
                "deleted_at BIGINT DEFAULT NULL, " +
                "INDEX product_image_url_idx(url), " +
                "FOREIGN KEY (product_id) REFERENCES product(id)" +
            ")");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "product");
            migrationBuilder.DropTable(name: "product_price");
            migrationBuilder.DropTable(name: "product_image");
        }
    }
}