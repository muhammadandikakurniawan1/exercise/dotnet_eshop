﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext.Migrations
{
    /// <inheritdoc />
    public partial class CreateStoreTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("CREATE TABLE store (" +
              "id varchar(255) NOT NULL PRIMARY KEY," +
              "name varchar(255) NOT NULL DEFAULT '', " +
              "user_id varchar(255) NOT NULL DEFAULT '', " +
              "picture TEXT NOT NULL, " +
              "header_picture TEXT NOT NULL, " +
              "created_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
              "updated_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
              "deleted_at BIGINT DEFAULT NULL, " +
              "INDEX store_name_idx(name)" +
            ")");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "store");
        }
    }
}