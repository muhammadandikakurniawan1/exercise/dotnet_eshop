﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext.Migrations
{
    /// <inheritdoc />
    public partial class AddGoogleAccountColumn : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                "ALTER TABLE app_user ADD google_account_id VARCHAR(255) NOT NULL DEFAULT ''" +
                "ALTER TABLE app_user ADD google_account_name VARCHAR(255) NOT NULL DEFAULT ''" +
                "ALTER TABLE app_user ADD google_account_email VARCHAR(255) NOT NULL DEFAULT ''" +
                "ALTER TABLE app_user ADD google_account_picture VARCHAR(255) NOT NULL DEFAULT ''");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropColumn(
                name: "google_account_email",
                table: "app_user");

            migrationBuilder.DropColumn(
                name: "google_account_id",
                table: "app_user");

            migrationBuilder.DropColumn(
                name: "google_account_name",
                table: "app_user");

            migrationBuilder.DropColumn(
                name: "google_account_picture",
                table: "app_user");
        }
    }
}
