﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext.Migrations
{
    /// <inheritdoc />
    public partial class CreateTableUser : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("CREATE TABLE app_user ("+
              "id varchar(255) NOT NULL PRIMARY KEY,"+
              "full_name varchar(255) NOT NULL DEFAULT '', "+
              "email varchar(255) NOT NULL DEFAULT '', " +
              "password TEXT NOT NULL, " +
              "created_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
              "updated_at BIGINT NOT NULL DEFAULT DATEDIFF_BIG(MILLISECOND, '1970-01-01 00:00:00.000', SYSUTCDATETIME()), " +
              "deleted_at BIGINT DEFAULT NULL, " +
              "INDEX app_user_full_name_idx(full_name), " +
              "INDEX app_user_email_idx(email)" +
            ")");

        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "app_user");
        }
    }
}
