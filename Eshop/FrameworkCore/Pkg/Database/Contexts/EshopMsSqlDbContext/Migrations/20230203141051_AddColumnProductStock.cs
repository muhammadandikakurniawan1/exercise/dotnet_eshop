﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext.Migrations
{
    /// <inheritdoc />
    public partial class AddColumnProductStock : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("ALTER TABLE product ADD stock INTEGER NOT NULL DEFAULT 0");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "stock",
                table: "product");
        }
    }
}
