﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Pkg.Models
{
    public class BaseResponseModel<T_DATA>
    {
        public T_DATA? Data { get; set; }
        public bool IsSuccess { get; set; } = false;
        public string Message { get; set; } = "";
        public string? ErrorMessage { get; set; }
        public string StatusCode { get; set; } = "";
        public HttpStatusCode HttpCode { get; set; }

        public BaseResponseModel<T_DATA> SetInternalServerError(Exception ex)
        {
            this.Message = "Internal server error";
            this.ErrorMessage = ex.InnerException?.Message ?? ex.Message;
            this.HttpCode = HttpStatusCode.InternalServerError;
            this.IsSuccess = false;
            this.StatusCode = "SVC500";

            return this;
        }
    }
}
