﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Pkg.FileStorageHelper;

public class SaveFileResult
{
    public string FileLocation { get; set; } = "";
    public string FileName { get; set; } = "";
    public long FileSize { get; set; } = 0;
    public string ContentType { get; set; } = "";
}

public class SaveFileOpt
{
    public byte[] FileByte { get; set; }
    public Stream FileStream { get; set; }
    public string FileName { get; set; } = "";
    public string Path { get; set; } = "";
    public string ContentType { get; set; } = "";
}