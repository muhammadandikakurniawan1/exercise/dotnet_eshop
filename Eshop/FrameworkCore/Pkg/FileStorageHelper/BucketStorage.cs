﻿using Minio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Pkg.FileStorageHelper;

public class BucketStorageClientOption
{
    public string Endpoint { get; set; } = "";
    public string PublicEndpoint { get; set; } = "";
    public string AccessKey { get; set; } = "";
    public string SecretKey { get; set; } = "";
    public bool UseSSL { get; set; } = false;
}
public class BucketStorageHelper : IFileStorageHelper
{

    private MinioClient _client;
    private BucketStorageClientOption _opt;

    public BucketStorageHelper(BucketStorageClientOption opt)
    {
        _opt = opt;
        _client = new MinioClient()
        .WithEndpoint(opt.Endpoint)
        .WithSSL(opt.UseSSL)
        .WithCredentials(opt.AccessKey, opt.SecretKey)
        .Build();
    }
    public async Task<SaveFileResult> SaveAsync(SaveFileOpt opt)
    {

        var path = opt.Path.Trim('/');
        var filename = opt.FileName.Trim('/');
        var bucketName = path;
        var pathChunk = path.Split("/");

        var isHaveNestedBucket = pathChunk.Length > 1;
        if (isHaveNestedBucket)
        {
            bucketName = pathChunk[0];
            var subDir = string.Join("/", pathChunk.Skip(1).ToArray()).Trim('/');
            filename = $"{filename}/{subDir}";
        }

        // Make a bucket on the server, if not already present.
        var beArgs = new BucketExistsArgs().WithBucket(bucketName);
        bool isBucketExists = await _client.BucketExistsAsync(beArgs).ConfigureAwait(false);
        if (!isBucketExists)
        {
            var mbArgs = new MakeBucketArgs().WithBucket(bucketName);
            await _client.MakeBucketAsync(mbArgs).ConfigureAwait(false);
        }
        // Upload a file to bucket.
        var putObjectArgs = new PutObjectArgs()
            .WithBucket(bucketName)
            .WithObject(filename)
            .WithObjectSize(opt.FileStream.Length)
            .WithStreamData(opt.FileStream)
            .WithContentType(opt.ContentType);
        await _client.PutObjectAsync(putObjectArgs).ConfigureAwait(false);


        return new SaveFileResult()
        {
            FileLocation = $"{this._opt.PublicEndpoint}/{bucketName}/{filename}",
            FileName = filename,
            ContentType = opt.ContentType,
            FileSize = opt.FileStream.Length
        };
    }
}
