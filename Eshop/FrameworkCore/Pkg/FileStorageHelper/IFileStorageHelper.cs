﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkCore.Pkg.FileStorageHelper
{
    public interface IFileStorageHelper
    {
        public Task<SaveFileResult> SaveAsync(SaveFileOpt opt);
    }
}
