namespace FrameworkCoreTest
{
    [TestClass]
    public class UnitTest1
    {
        private string base64Str = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEAlgCWAAD/4QD0RXhpZgAATU0AKgAAAAgABQEOAAIAAACRAAAASgEaAAUAAAABAAAA3AEbAAUAAAABAAAA5AEoAAMAAAABAAIAAAITAAMAAAABAAEAAAAAAABDdXRlIEFzdHJvbmF1dCBIb2xkaW5nIEZsYWcgT24gTW9vbiBDYXJ0b29uIFZlY3RvciBJY29uIElsbHVzdHJhdGlvbiBTY2llbmNlIFRlY2hub2xvZ3kgSWNvbiBDb25jZXB0IElzb2xhdGVkIFByZW1pdW0gVmVjdG9yLiBGbGF0IENhcnRvb24gU3R5bGUAAAAAAJYAAAABAAAAlgAAAAH/7QMsUGhvdG9zaG9wIDMuMAA4QklNA";

        [TestMethod]
        public void TestMethod1()
        {
            var base64Chunk = base64Str.Split(";base64,");

            var contentType = base64Chunk[0].Replace("data:", "");
            var base64Strs = base64Chunk[1];

            Console.WriteLine(base64Chunk);
        }

        [TestMethod]
        public void TestSearchIdx()
        {
            var datas = new List<string>() { "satu", "dua", "tiga", "empat" };

            var idx = datas.FindIndex(d => d.Equals("tiga"));
            idx = datas.FindIndex(d => d.Equals("asdasd"));

            Console.WriteLine(idx);
        }
    }


}