﻿using FrameworkCore.Domains.User.ValueObjects;

namespace WebbApp.Models.Auth
{
    public class LoginViewModel
    {
        public UserType LoginType { get; set; }
        public string AccessToken { get; set; } = "";
        public string Username { get; set; } = "";
        public string Email { get; set; } = "";
        public string Password { get; set; } = "";
    }
}
