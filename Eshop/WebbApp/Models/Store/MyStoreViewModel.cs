﻿using FrameworkCore.Domains.Store.Entities;
using FrameworkCore.Domains.Store.Usecases.Store.Models;

namespace WebbApp.Models.Store;

public class MyStoreViewModel
{
    public string Id { get; set; } = "";
    public string HeaderPictBase64 { get; set; } = "";
    public string ProfilleBase64 { get; set; } = "";
    public string HeaderPictUrl { get; set; } = "";
    public string ProfileUrl { get; set; } = "";
    public string Name { get; set; } = "";
    public string UserId { get; set; } = "";

    public List<ProductEntity> ListProduct = new List<ProductEntity>();

    public MyStoreViewModel SetFromStoreEntity(StoreEntity storeEntity)
    {
        this.HeaderPictUrl = storeEntity.HeaderPicture;
        this.ProfileUrl = storeEntity.Picture;
        this.Name = storeEntity.Name;
        if(storeEntity.Id != null) this.Id = storeEntity.Id.ToString()!;

        return this;
    }

    public CreateNewStoreRequest ToCreateStoreRequestModel()
    {
        var result = new CreateNewStoreRequest();
        result.HeaderPictBase64 = this.HeaderPictBase64;
        result.ProfilePictBase64 = this.ProfilleBase64;
        result.UserId = this.UserId;
        result.StoreName = this.Name;
        return result;
    }
}
