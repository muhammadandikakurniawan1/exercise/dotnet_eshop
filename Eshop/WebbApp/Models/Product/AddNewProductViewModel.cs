﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace WebbApp.Models.Product;

public class AddNewProductViewModel
{
    public string StoreId { get; set; } = "";
    public string Name { get; set; } = "";

    //public IFormFile[] PictureFiles { get; set; }
    public HttpPostedFileBaseModelBinder[] PictureFiles { get; set; }

        
}
