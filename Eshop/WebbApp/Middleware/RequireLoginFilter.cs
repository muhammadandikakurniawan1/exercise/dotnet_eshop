﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Security.Claims;
using System.Security.Principal;
using WebbApp.Controllers;
using FrameworkCore.Domains.User.Entities;
using Microsoft.AspNetCore.Authorization;
using FrameworkCore.Domains.User.Usecases.User;

namespace WebbApp.Middleware
{
    public class RequireLoginFilter : AuthorizeAttribute, IAuthorizationFilter
    {
        private IUserUsecase _userUsecase;

        public RequireLoginFilter(IUserUsecase userUsecase)
		{
            _userUsecase = userUsecase;

        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {

            var endpoint = context.HttpContext.GetEndpoint();
            var isAllowAnonymEndpoint = endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() != null;

            var sessionLoginData = new byte[] { };
            var sessionLoginIsExists = context.HttpContext.Session.TryGetValue(Constanta.SESSION_USER_LOGIN_KEY, out sessionLoginData);
            
            if (sessionLoginIsExists || isAllowAnonymEndpoint)
            {
                return;
            }

            context.Result = new RedirectToRouteResult(new RouteValueDictionary(new
            {
                controller = "auth",
                action = "login",
            }));

        }
    }
}
