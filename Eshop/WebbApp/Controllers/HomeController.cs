﻿using FrameworkCore.Domains.Store.Usecases.Product;
using FrameworkCore.Domains.Store.Usecases.Product.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebbApp.Models;


namespace WebbApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IProductUsecase _productUsecase;

        public HomeController(
            ILogger<HomeController> logger,
            IProductUsecase productUsecase
        )
        {
            _logger = logger;
            _productUsecase = productUsecase;
        }

        public async Task<IActionResult> Index()
        {
            //var paginationReq = new ProductPaginationModel()
            //{
            //    CurrentPage = 1,
            //    SearchKeyword = "boba"
            //};
            //var res = await _productUsecase.GetProductPagination(paginationReq);
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}