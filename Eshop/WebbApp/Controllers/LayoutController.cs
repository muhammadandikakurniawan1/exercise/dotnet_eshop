﻿//using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc;

namespace WebbApp.Controllers
{
    public class LayoutController : Controller
    {
        [System.Web.Mvc.ChildActionOnly]
        public IActionResult RenderCmsLayout()
        {
            return View("~/Views/Shared/_SbAdminCmsLayout.cshtml");
        }
    }
}
