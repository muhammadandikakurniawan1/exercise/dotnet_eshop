﻿using FrameworkCore.Domains.Store.Usecases.Store;
using FrameworkCore.Domains.User.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WebbApp.Controllers;

public class BaseController : Controller
{
    protected UserEntity GetUserLoginData()
    {
        var userLoginDataStr = HttpContext.Session.GetString(Constanta.SESSION_USER_LOGIN_KEY);
        if (userLoginDataStr == null) throw new Exception("user login not found");
        var userLoginData = JsonConvert.DeserializeObject<UserEntity>(userLoginDataStr!)!;

        return userLoginData;
    }
}
