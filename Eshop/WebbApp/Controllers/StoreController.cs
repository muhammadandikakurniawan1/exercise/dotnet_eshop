﻿using FrameworkCore.Domains.Store.Usecases.Store;
using FrameworkCore.Domains.Store.Usecases.Store.Models;
using FrameworkCore.Domains.User.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebbApp.Models.Store;

namespace WebbApp.Controllers
{
    [Controller]
    [Route("store")]
    public class StoreController : BaseController
    {
        private IStoreUsecase _storeUsecase;
        public StoreController(IStoreUsecase storeUsecase)
        {
            _storeUsecase = storeUsecase;
        }

        [Route("mystore")]
        public async Task<IActionResult> MyStoreViewAsync()
        {

            var userLoginData = GetUserLoginData();
            var model = new WebbApp.Models.Store.MyStoreViewModel();
            model.UserId = userLoginData.Id.ToString()!;

            var getMyStoreResult = await _storeUsecase.GetMyStore(userLoginData.Id!.ToString()!);
            if(getMyStoreResult.HttpCode == System.Net.HttpStatusCode.NoContent)
            {
                return View("~/Views/Store/CreateMyStore.cshtml",  model);
            }
            model.SetFromStoreEntity(getMyStoreResult.Data!);
            return View("~/Views/Store/MyStore.cshtml", model);
        }

        [HttpPost("create-store")]
        public async Task<IActionResult> CreateStoreProcess(MyStoreViewModel model)
        {
            var userLoginData = GetUserLoginData();

            var userCaseReq = model.ToCreateStoreRequestModel();
            userCaseReq.UserId = userLoginData.Id.ToString()!;
            var result = await _storeUsecase.CreateStore(userCaseReq);

            model.SetFromStoreEntity(result.Data!);
            return new RedirectToRouteResult(new RouteValueDictionary(new
            {
                controller = "store",
                action = "mystore",
            }));
        }


    }
}
