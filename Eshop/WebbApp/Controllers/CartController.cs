﻿using FrameworkCore.Domains.Cart.Entities;
using FrameworkCore.Domains.Cart.Usecases.Cart;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace WebbApp.Controllers;

[Route("cart")]
[Controller]
public class CartController : BaseController
{
    private ICartUsecase _cartUsecase;
    public CartController(ICartUsecase cartUsecase)
    {
        _cartUsecase = cartUsecase;
    }

    [AllowAnonymous]
    [HttpPost("add-item")]
    public async Task<JsonResult> AddItem([FromBody] CartItemEntity model)
    {
        var userLoginData = GetUserLoginData();
        model.UserId = userLoginData.Id.ToString()!;
        var result = await _cartUsecase.AddItem(model);

        return Json(result);
    }

    public async Task<IActionResult> Index()
    {
        
        var userLoginData = GetUserLoginData();
        var result = await _cartUsecase.GetByUserId(userLoginData.Id.ToString()!);
        var itemPerStore = result.Data!.GetItemPerStore();
        return View("~/Views/Cart/Index.cshtml", itemPerStore);
    }
}
