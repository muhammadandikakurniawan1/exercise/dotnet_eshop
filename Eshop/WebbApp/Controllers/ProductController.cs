﻿using FrameworkCore.Domains.Store.Entities;
using FrameworkCore.Domains.Store.Usecases.Product;
using FrameworkCore.Domains.Store.Usecases.Store;
using Microsoft.AspNetCore.Mvc;

namespace WebbApp.Controllers
{
    [Controller]
    public class ProductController : BaseController
    {
        private IStoreUsecase _storeUsecase;
        private IProductUsecase _productUsecase;
        public ProductController(IStoreUsecase storeUsecase, IProductUsecase productUsecase)
        {
            _storeUsecase = storeUsecase;
            _productUsecase = productUsecase;
        }

        [HttpGet("store/{storeId}/add-product")]
        public async Task<IActionResult> AddNewProductView(string storeId)
        {
            var userLoginData = GetUserLoginData();

            var getMyStoreResult = await _storeUsecase.GetMyStore(userLoginData.Id!.ToString()!);
            if (!getMyStoreResult.IsSuccess)
            {
                throw new Exception("failed get data store");
            }
            if (!getMyStoreResult.Data!.Id.ToString()!.Equals(storeId))
            {
                throw new Exception("store not found");
            }

            var model = new ProductEntity();
            model.StoreId = Guid.Parse(storeId);
            return await Task.FromResult(View("~/Views/Product/AddNewProduct.cshtml", model));
        }

        [HttpGet("{productId}/product")]
        public async Task<IActionResult> ProductDetailVew(int productId)
        {
            var result = await _productUsecase.GetProductDetail(productId);
            return View("~/Views/Product/ProductDetail.cshtml", result.Data);
        }

        [HttpPost("add-product-process")]
        public async Task<IActionResult> AddNewProductProcess(ProductEntity model)
        {

            var result = await _storeUsecase.CreateNewProduct(model);
            if (!result.IsSuccess) throw new Exception(result.ErrorMessage);
            var routeValue = new RouteValueDictionary(new { controller = "Product" , action = "ProductDetailVew", productId = result.Data.Id});
            return await Task.FromResult(RedirectToRoute(routeValue));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ShowListProductView([System.Web.Http.FromUri] string searchKeyword, [System.Web.Http.FromUri] int currentPage)
        {
            return ViewComponent("ShowListProduct", new { searchKeyword = searchKeyword, currentPage = currentPage }); ;
            //return PartialView("~/Views/Product/_ListProduct.cshtml", res.Data!.ListProduct);
        }
    }
}
