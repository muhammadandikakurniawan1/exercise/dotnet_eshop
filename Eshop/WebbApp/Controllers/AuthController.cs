﻿using FrameworkCore.Domains.User.Entities;
using FrameworkCore.Domains.User.Usecases.User;
using FrameworkCore.Domains.User.Usecases.User.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebbApp.Models.Auth;

namespace WebbApp.Controllers
{
    [Controller]
    [Route("auth")]
    public class AuthController : Controller
    {
        private IUserUsecase _userUsecase;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AuthController(
            IUserUsecase userUsecase
            , IHttpContextAccessor httpContextAccessor)
        {
            _userUsecase = userUsecase;
            _httpContextAccessor = httpContextAccessor;
        }

        [AllowAnonymous]
        [HttpGet("login")]
        public IActionResult LoginView()
        {
            var model = new LoginViewModel();
            return View("~/Views/Auth/Login.cshtml", model);
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> LoginProcess(LoginViewModel model)
        {
            var errors = new List<string>();
            try
            {
                var usecaseReq = new LoginOAuthRequestModel()
                {
                    LoginType = model.LoginType,
                    AccessToken = model.AccessToken,
                };
                var usecaseRes = await _userUsecase.LoginOauthAsync(usecaseReq);
                if (usecaseRes.IsSuccess) {
                    var sessionData = Newtonsoft.Json.JsonConvert.SerializeObject(usecaseRes.Data);
                    HttpContext.Session.SetString(Constanta.SESSION_USER_LOGIN_KEY, sessionData);
                    var routeValue = new RouteValueDictionary(new { action = "Index", controller = "Home" });
                    return RedirectToRoute(routeValue);
                }

                errors.Add(usecaseRes.Message);

            }
            catch(Exception ex)
            {
                errors.Add(ex.Message);
            }

            ViewBag.Errors = errors;

            return View("~/Views/Auth/Login.cshtml");
        }

        [Route("logout")]
        public IActionResult LogoutView()
        {
            return View("~/Views/Auth/Logout.cshtml");
        }

        [Route("logout-process")]
        public IActionResult LogoutProcess()
        {
            HttpContext.Session.Remove(Constanta.SESSION_USER_LOGIN_KEY);
            var routeValue = new RouteValueDictionary(new { action = "LoginView", controller = "Auth" });
            return RedirectToRoute(routeValue);
        }
    }
}
