﻿using FrameworkCore.Domains.User.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WebbApp.ViewComponents
{
    public class CmsTopBarViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {

            var userLoginDataStr = HttpContext.Session.GetString(Constanta.SESSION_USER_LOGIN_KEY);

            var userLoginData = JsonConvert.DeserializeObject<UserEntity>(userLoginDataStr!)!;

            return await Task.FromResult((IViewComponentResult)View("~/Views/Shared/_SbAdminCmsTopBarLayout.cshtml", userLoginData!));
        }
    }
}
