﻿using FrameworkCore.Domains.Store.Usecases.Product;
using FrameworkCore.Domains.Store.Usecases.Product.Models;
using FrameworkCore.Domains.Store.Usecases.Store;
using FrameworkCore.Domains.User.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WebbApp.ViewComponents;

//[ViewComponent("ShowListProduct")]
public class ShowListProductViewComponent : ViewComponent
{

    private IStoreUsecase _storeUsecase;
    private IProductUsecase _productUsecase;
    public ShowListProductViewComponent(IStoreUsecase storeUsecase, IProductUsecase productUsecase)
    {
        _storeUsecase = storeUsecase;
        _productUsecase = productUsecase;
    }
    public async Task<IViewComponentResult> InvokeAsync(string searchKeyword = "", int currentPage = 1)
    {

        var paginationReq = new ProductPaginationModel()
        {
            CurrentPage = currentPage,
            SearchKeyword = searchKeyword
        };
        var res = await _productUsecase.GetProductPagination(paginationReq);
        return await Task.FromResult((IViewComponentResult)View("~/Views/Product/_ListProduct.cshtml", res.Data!.ListProduct));
    }
}
