using FrameworkCore.Domains.Cart.Infrastructure.Persistence;
using FrameworkCore.Domains.Cart.Repositories;
using FrameworkCore.Domains.Cart.Usecases.Cart;
using FrameworkCore.Domains.Store.Infrastructure.Persistence;
using FrameworkCore.Domains.Store.Repositories;
using FrameworkCore.Domains.Store.Usecases.Product;
using FrameworkCore.Domains.Store.Usecases.Store;
using FrameworkCore.Domains.User.Infrastructure.Persistence;
using FrameworkCore.Domains.User.Infrastructure.Persistence.Dao;
using FrameworkCore.Domains.User.Infrastructure.Service.Google;
using FrameworkCore.Domains.User.Repositories;
using FrameworkCore.Domains.User.Usecases.User;
using FrameworkCore.Pkg.Database.Contexts.EshopMsSqlDbContext;
using FrameworkCore.Pkg.FileStorageHelper;
using Microsoft.EntityFrameworkCore;
using Polly;
using Polly.Extensions.Http;
using WebbApp.Middleware;

var builder = WebApplication.CreateBuilder(args);

IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
{
    return HttpPolicyExtensions
      // Handle HttpRequestExceptions, 408 and 5xx status codes
      .HandleTransientHttpError()
      .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
      .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.Unauthorized)
      .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.InternalServerError)
      // What to do if any of the above erros occur:
      // Retry 3 times, each time wait 1,2 and 4 seconds before retrying.
      .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
}

// Add framework services.
builder.Services
    .AddHttpContextAccessor()
    .AddSession()
	.AddControllersWithViews()
    // Maintain property names during serialization. See:
    // https://github.com/aspnet/Announcements/issues/194
    .AddJsonOptions(o =>
    {
        o.JsonSerializerOptions.PropertyNamingPolicy = null;
        o.JsonSerializerOptions.DictionaryKeyPolicy = null;
    });
//.AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver());

builder.Services.AddHttpClient("HttpClient").AddPolicyHandler(GetRetryPolicy());

// Add Kendo UI services to the services container
builder.Services.AddKendo();

var configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

var msSqlConnStr = configuration.GetValue<string>("Database:MsSQL");
builder.Services.AddDbContext<EshopMsSqlDbContext>(options => options.UseSqlServer(msSqlConnStr), ServiceLifetime.Scoped);

builder.Services.AddSingleton(configuration);

// START register repositories 
builder.Services.AddScoped<IAppUserDao, AppUserMsSqlDaoImpl>();
builder.Services.AddScoped<IAccountRepository, AccountRepositoryMsSqlImpl>();
builder.Services.AddScoped<IStoreRepository, StoreRepositoryMsSqlImpl>();
builder.Services.AddScoped<IProductRepository, ProductRepositoryMsSqlImpl>();
builder.Services.AddScoped<ICartRepository, CartRepositoryMsSqlImpl>();
// END register repositories 

// START register usecase
builder.Services.AddScoped<IUserUsecase, UserUsecase>();
builder.Services.AddScoped<IStoreUsecase, StoreUsecase>();
builder.Services.AddScoped<IProductUsecase, ProductUsecase>();
builder.Services.AddScoped<ICartUsecase, CartUsecase>();
// END register usecase

// START register infrastructure service 
builder.Services.AddScoped<IGoogleOAuth2Service, GoogleOAuth2Service>();
builder.Services.AddScoped<FrameworkCore.Domains.Store.Infrastructure.Service.User.IUserUsecase, FrameworkCore.Domains.Store.Infrastructure.Service.User.UserUsecaseSqlServerImpl>();
builder.Services.AddScoped<FrameworkCore.Domains.Cart.Infrastructure.Service.User.IUserUsecase, FrameworkCore.Domains.Cart.Infrastructure.Service.User.UserUsecaseSqlServerImpl>();
builder.Services.AddScoped<FrameworkCore.Domains.Cart.Infrastructure.Service.Store.IProductUsecase, FrameworkCore.Domains.Cart.Infrastructure.Service.Store.ProductUsecaseSqlServerImpl>();
// END register infrastructure service 

// START register helper
builder.Services.AddSingleton<IFileStorageHelper>(new BucketStorageHelper(new BucketStorageClientOption()
{
    Endpoint = configuration.GetValue<string>("BucketStorage:Endpoint"),
    AccessKey = configuration.GetValue<string>("BucketStorage:AccessKey"),
    SecretKey = configuration.GetValue<string>("BucketStorage:SecretKey"),
    UseSSL = configuration.GetValue<bool>("BucketStorage:UseSSL"),
    PublicEndpoint = configuration.GetValue<string>("BucketStorage:PublicEndpoint")
}));
// END register helper

builder.Services.AddControllers(config =>
{
    config.Filters.Add<RequireLoginFilter>();
});

// Add services to the container.

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSession();
app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

