﻿$(document).ready(function () {
});

function previewImage(e, previewId, inputbase64StrId) {
    if (e.target.files.length > 0) {
        let file = e.target.files[0];
        var src = URL.createObjectURL(file);

        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            $(`#${inputbase64StrId}`)[0].value = reader.result
            setTimeout(() => {
                $(`#${previewId}`)[0].src = src;
            },100)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
}

function openFile(id) {
    $(`#${id}`).click()
}