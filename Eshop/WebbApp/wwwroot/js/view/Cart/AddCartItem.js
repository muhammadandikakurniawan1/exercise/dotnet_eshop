﻿function addCartItem(productId, qty) {
    var token = $('input[name="__RequestVerificationToken"]').val();
    const url = `${window.location.origin}/cart/add-item`;
    const data = {
        "__RequestVerificationToken": token,
        "product": {
            "id": productId
        },
        "qty": qty
    };
    $.ajax({
        url: url,
        type: "post",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(data),
        complete: function (result) {
            console.log(result);
        }
    });
}