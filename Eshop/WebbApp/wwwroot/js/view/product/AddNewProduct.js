﻿$(document).ready(() => {

})

function showLoadingScreen(isShow) {
    if (isShow) {
        document.querySelector(".loading-screen").classList.remove("d-none")
    } else {
        document.querySelector(".loading-screen").classList.add("d-none")
    }
}

function previewImage(e) {
    $(".carousel-item").remove();
    if (e.target.files.length > 0) {

        for (var fileIdx = 0; fileIdx < e.target.files.length; fileIdx++) {
            let file = e.target.files[fileIdx];
            var src = URL.createObjectURL(file);
            let fileType = file.type.split("/")[0];
            let activeClas = fileIdx == 0 ? 'active' : '';
            let content = ``


            if (fileType == "image") {
                content = `<div class="carousel-item ${activeClas}"><img class="d-block w-100" src="${src}" alt="First slide"></div>`
            } else if (fileType == "video") {
                content = `<div class="carousel-item ${activeClas}"><video class="d-block w-100" controls><source src="${src}"></video></div>`
            }

            $("#postFilePreview")[0].innerHTML += content
            $(".carousel-indicators")[0].innerHTML += ` <li data-target="#postFilePreviewContainer" data-slide-to="${fileIdx}" class=" ${activeClas}"></li>`
        }
        $("#postFilePreviewContainer")[0].classList.remove("d-none")
    } else {
        $("#postFilePreviewContainer")[0].classList.add("d-none")
    }
}

function readFileAsBase64(file) {
    return new Promise((resolve, reject) => {
        let reader = new FileReader();
        reader.readAsDataURL(file)
        reader.onload = function () {
            let res = reader.result;
            console.log(res)
            resolve(res.split("base64,")[1])
        }
        reader.onerror = (err) => {
            reject(err)
        }
    })
}

function setAlert(type, message) {
    document.querySelector("#alertsContainer").innerHTML += `
                    <div class="alert ${type} alert-dismissible fade show" role="alert">
                        <p>${message}</p>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>`
}

function setupListFile() {
    return new Promise((resolve, reject) => {
        let filesRequestData = [];
        let files = document.querySelector("#inputFile").files;

        let timeOut = 10 * files.length;

        for (var fileIdx = 0; fileIdx < files.length; fileIdx++) {
            let file = files[fileIdx];
            let fileDataReq = {
                "name": file.name,
                "type": file.type
            }

            readFileAsBase64(file).then(res => {
                fileDataReq["base64"] = res
                filesRequestData.push(fileDataReq)
            })
        }
        setTimeout(() => {
            resolve(filesRequestData);
        }, timeOut);
    })
}

function submitPost() {
    // setupListFile()
    // .then((dataFiles) => {
    //     let dataRequest = {
    //         "files": dataFiles || [],
    //         "caption" : document.querySelector("#postCaption").value
    //     };

    //     console.log(dataFiles);

    showLoadingScreen(true)
    //     $.ajax({
    //         contentType: 'application/json',
    //         type: "POST",
    //         dataType: 'json',
    //         url : "<?= base_url("PostController/createProcess") ?>",
    //         data: JSON.stringify(dataRequest),
    //         success: (data) =>{
    //             console.log("============ success response ==============")
    //             console.log(data)
    //             showLoadingScreen(false)
    //             setAlert("alert-success","Submit Post Success")

    //         },
    //         error : (err) => {
    //             setAlert("alert-danger","internal error")
    //             console.log("================ submit post error ==================")
    //             console.log(err)
    //             showLoadingScreen(false)
    //         }
    //     });
    // })
    // .catch((err) => {
    //     setAlert("alert-danger","internal error")
    //     console.log("================ submit post error ==================")
    //     console.log(err) 
    // })
}
